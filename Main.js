import { GameCanvas } from './GameCanvas';
export class MainLoop {
    static mainLoop(timestamp) {
        if (MainLoop.pause) {
            requestAnimationFrame(MainLoop.mainLoop);
            return;
        }
        if (timestamp < MainLoop.lastFrameTimeMs + (1000 / MainLoop.maxFPS)) {
            requestAnimationFrame(MainLoop.mainLoop);
            return;
        }
        MainLoop.lastFrameTimeMs = timestamp;
        board.update();
        board.drawBoard();
        requestAnimationFrame(MainLoop.mainLoop);
    }
}
MainLoop.lastFrameTimeMs = 0;
MainLoop.maxFPS = 3; // The last time the loop was run, max fps
MainLoop.pause = false;
let canvas = document.getElementById('canvas'); //ge the canvas from HTML document
let previewCanvas = document.getElementById('previewCanvas'); //ge the canvas from HTML document
export let board = new GameCanvas(canvas, previewCanvas);
window.addEventListener('keydown', function (e) {
    if (!MainLoop.pause) {
        board.keyHandle(String.fromCharCode(e.which), board);
    }
}); //setup eventlistener
window.addEventListener('keydown', function (e) {
    if (String.fromCharCode(e.which) == "P") {
        if (MainLoop.pause) {
            MainLoop.pause = false;
            return;
        }
        MainLoop.pause = true;
    }
}); //setup eventlistener
requestAnimationFrame(MainLoop.mainLoop);
//dont even need async, just add eventlistener and a function thats binded to it
