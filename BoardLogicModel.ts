import { Block } from './IPieceMeta'
import { PieceAbstraction } from './PieceAbstraction';

export enum MatrixEntry {
    EMPTY = 0,
    FILLED = 1
}

//rather than typing MatrixEntry.EMPTY
const EMPTY = MatrixEntry.EMPTY;

export type CoordinatePair = {
    x: number,
    y: number
}

export class boardLogicModel {
    //16 rows, 10 columns
    public BoardMatrix: Block[][] = [
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
        [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], //rows with individual entries
    ];



    constructor(private readonly TILE_HEIGHT, private readonly TILE_WIDTH) {
    }

    public addPieceToBoard(piece: PieceAbstraction) {
        //Adding pieces to the Map && Matrix
        for (let i = 0; i < piece.pieceConstructed.blocksContainer.length; i++) {
            let matrixY = this.getMatrixY(piece.pieceConstructed.blocksContainer[i].yPosition);
            let matrixX = this.getMatrixX(piece.pieceConstructed.blocksContainer[i].xPosition);
            this.BoardMatrix[matrixY][matrixX] = piece.pieceConstructed.blocksContainer[i]; //y is row, x is column
        }
    }


    public getMatrix() {
        return this.BoardMatrix;
    }

    public getMatrixY(canvasY: number) {
        return canvasY / this.TILE_HEIGHT;
    }


    public getMatrixX(canvasX: number) {
        return canvasX / this.TILE_WIDTH;
    }


    public blockMatrixInteraction(block: Block, canMove: Function) {
        let matrixY = this.getMatrixY(block.yPosition);
        let matrixX = this.getMatrixX(block.xPosition);
        canMove(matrixY, matrixX);
    }


    public vectorizePoint(block: Block): number[] {
        let matrixY = this.getMatrixY(block.yPosition);
        let matrixX = this.getMatrixX(block.xPosition);
        return [matrixY, matrixX];
    }


    public handleRowCleanup() {
        //Go through each row in the matrix. Check if row is full
        //store the row it is in an array
        //sort that array from greatest to least (bottom to top)

        //remove all of the blocks in that array, set the whole row to undefined


        //loop through from the bottom to next highest
        //check if the row above the row we started from is full, (peak into the array)
        //if it is, then set tiledown to + this.tile_height * x (where x is the number of consecutive full arrays)
        //push the array down to currentRow + x rows (remmeber concurrent)
        //inside each row of the array, make sure you change the this.tileHeight to be aligned with the matrix
        //so if you moved it two rows down, Block.yPosition += x * this.tile_height

        let rowsFilled = [];

        //Here we are adding all full rows
        for (let row = 0; row < this.BoardMatrix.length; row++) {
            for (let tile = 0; tile < this.BoardMatrix[row].length; tile++) {
                if (this.BoardMatrix[row][tile] === undefined) {
                    break;
                } else if (tile == this.BoardMatrix[row].length - 1) {
                    rowsFilled.push(row);
                }
            }
        }

        for (let row = 0; row < rowsFilled.length; row++) {
            for (let tile = 0; tile < this.BoardMatrix[rowsFilled[row]].length; tile++) {
                this.BoardMatrix[rowsFilled[row]][tile] = undefined;
            }
        }


        rowsFilled = rowsFilled.sort((a, b) => b - a); //sorting the array from greatest to least (bottom to top)

        if (rowsFilled.length > 0) {
            //iterating through each row
            for (let i = 0; i < rowsFilled.length; i++) {
                for (let rowToShift = rowsFilled[i] - 1; rowToShift >= 0; rowToShift--) { //rowToShift == 14

                    //iteration to check for consecutive rows
                    let x = 1;
                    //Checking for consecutive rows
                    while (rowToShift == rowsFilled[i + x]) {
                        rowToShift--;
                        x++;
                    }

                    for (let tile = 0; tile < this.BoardMatrix[rowToShift].length; tile++) {
                        if (this.BoardMatrix[rowToShift][tile] !== undefined) {
                            let temp = this.BoardMatrix[rowToShift][tile]; //Temp object
                            temp.yPosition += x * this.TILE_HEIGHT;
                            this.BoardMatrix[rowToShift][tile] = undefined;

                            this.BoardMatrix[rowToShift + x][tile] = temp;
                        }
                    }
                }
            }
        }
    }
}

