import { PieceAbstraction } from "./PieceAbstraction";
export class RightZPiece extends PieceAbstraction {
    constructor(TILE_WIDTH, TILE_HEIGHT, BOARD_HEIGHT, canvas) {
        super();
        this.TILE_WIDTH = TILE_WIDTH;
        this.TILE_HEIGHT = TILE_HEIGHT;
        this.BOARD_HEIGHT = BOARD_HEIGHT;
        this.canvas = canvas;
        this.COLOR = "Blue";
        this.topRightBlock = {
            previewY: 25,
            previewX: 75,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 6,
            yPosition: 0,
            width: this.TILE_WIDTH,
            height: this.TILE_HEIGHT,
            color: this.COLOR
        };
        this.topMiddleBlock = {
            previewY: 25,
            previewX: 50,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 5,
            yPosition: 0,
            width: this.TILE_WIDTH,
            height: this.TILE_HEIGHT,
            color: this.COLOR
        };
        this.bottomMiddleBlock = {
            previewY: 50,
            previewX: 50,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 5,
            yPosition: this.TILE_HEIGHT,
            width: this.TILE_WIDTH,
            height: this.TILE_HEIGHT,
            color: this.COLOR
        };
        this.bottomLeftBlock = {
            previewY: 50,
            previewX: 25,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 4,
            yPosition: this.TILE_HEIGHT,
            width: this.TILE_WIDTH,
            height: this.TILE_HEIGHT,
            color: this.COLOR
        };
        this.pieceConstructed = { amountOfBlocks: 4, color: this.COLOR, blocksContainer: [this.topRightBlock, this.topMiddleBlock, this.bottomMiddleBlock, this.bottomLeftBlock] };
        this.pieceRotationProperties = { isRotatable: true, axisOfRotation: this.bottomMiddleBlock };
        this.pieceInitalized = true;
    }
    updateBoardPosition() {
        if (this.pieceInitalized) {
            super.downMovement(this.canvas);
        }
    }
    getPiece() {
        return this.pieceConstructed;
    }
}
