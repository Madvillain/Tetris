import { LLPiece } from './LeftLPiece';
import { RLPiece } from './RightLPiece';
import { SquarePiece } from './SquarePiece';
import { RightZPiece } from './RightZPiece';
import { LeftZPiece } from './LeftZPiece';
import { StickPiece } from './StickPiece';
import { TBlock } from './t-block';
import { boardLogicModel } from './BoardLogicModel';
export class GameCanvas {
    constructor(canvas, previewCanvas) {
        this.canvas = canvas;
        this.previewCanvas = previewCanvas;
        this.LEFT_MARGIN = 0;
        this.RIGHT_MARGIN = GameCanvas.BOARD_WIDTH - GameCanvas.TILE_WIDTH;
        this.BOTTOM_MARGIN = GameCanvas.BOARD_HEIGHT - GameCanvas.TILE_HEIGHT;
        this.VALID_KEYS = ['w', 'a', 's', 'd', 'e'];
        this.livePiece = undefined;
        this.lastTwoPieces = [];
        this.LOCK = false;
        this.lastMoveTaken = false;
        this.firstPieceSelected = false;
        //Main Canvas
        this.canvas.height = GameCanvas.BOARD_HEIGHT;
        this.canvas.width = GameCanvas.BOARD_WIDTH;
        //PreviewCanvas
        this.previewCanvas.height = 125;
        this.previewCanvas.width = 125;
        this.boardLogic = new boardLogicModel(GameCanvas.TILE_HEIGHT, GameCanvas.TILE_WIDTH); //initalizing boardLogicController
        this.drawBoard();
    }
    //Draws board with tiles
    drawBoard() {
        var _a, _b;
        //Setup Preview
        let previewContext = this.previewCanvas.getContext("2d");
        previewContext.clearRect(0, 0, 125, 125);
        if (this.nextPiece) {
            for (let i = 0; i < this.nextPiece.pieceConstructed.amountOfBlocks; i++) {
                drawBorderForPreview(previewContext, this.nextPiece.pieceConstructed.blocksContainer[i]);
                previewContext.fillStyle = (_a = this.nextPiece.pieceConstructed.color) !== null && _a !== void 0 ? _a : "Black";
                previewContext.fillRect(this.nextPiece.pieceConstructed.blocksContainer[i].previewX, this.nextPiece.pieceConstructed.blocksContainer[i].previewY, this.nextPiece.pieceConstructed.blocksContainer[i].previewWidth, this.nextPiece.pieceConstructed.blocksContainer[i].previewHeight);
            }
        }
        //Draw Main Board on Canvas
        let context = this.canvas.getContext("2d");
        context.clearRect(0, 0, GameCanvas.BOARD_WIDTH, GameCanvas.BOARD_HEIGHT);
        let squareWidth = GameCanvas.BOARD_WIDTH / 10;
        let sqaureHeight = GameCanvas.BOARD_HEIGHT / 16;
        let yPosition = 0;
        for (let i = 0; i < 16; i++) {
            let xPosition = 0;
            for (let j = 0; j < 10; j++) {
                context.strokeStyle = "Black";
                context.strokeRect(xPosition, yPosition, squareWidth, sqaureHeight);
                if (xPosition <= GameCanvas.BOARD_WIDTH - 25) {
                    xPosition += squareWidth;
                }
                else {
                    //we've drawn the whole row so now move onto next
                    break;
                }
            }
            yPosition += sqaureHeight;
        }
        //Draw Pieces coming down
        if (this.livePiece !== undefined) {
            for (let i = 0; i < this.livePiece.pieceConstructed.amountOfBlocks; i++) {
                drawBorder(context, this.livePiece.pieceConstructed.blocksContainer[i]);
                context.fillStyle = (_b = this.livePiece.pieceConstructed.color) !== null && _b !== void 0 ? _b : "Black";
                context.fillRect(this.livePiece.pieceConstructed.blocksContainer[i].xPosition, this.livePiece.pieceConstructed.blocksContainer[i].yPosition, this.livePiece.pieceConstructed.blocksContainer[i].width, this.livePiece.pieceConstructed.blocksContainer[i].height);
            }
        }
        //Draw blocks already set
        for (let row = 0; row < this.boardLogic.BoardMatrix.length; row++) {
            for (let tile = 0; tile < this.boardLogic.BoardMatrix[row].length; tile++) {
                if (this.boardLogic.BoardMatrix[row][tile] !== undefined) {
                    drawBorder(context, this.boardLogic.BoardMatrix[row][tile]);
                    context.fillStyle = this.boardLogic.BoardMatrix[row][tile].color;
                    context.fillRect(this.boardLogic.BoardMatrix[row][tile].xPosition, this.boardLogic.BoardMatrix[row][tile].yPosition, this.boardLogic.BoardMatrix[row][tile].width, this.boardLogic.BoardMatrix[row][tile].height);
                }
            }
        }
        function drawBorderForPreview(context, block, thickness = 1) {
            context.fillStyle = 'White';
            context.fillRect(block.previewX - (thickness), block.previewY - (thickness), block.previewWidth + (thickness * 2), block.previewHeight + (thickness * 2));
        }
        function drawBorder(context, block, thickness = 1) {
            context.fillStyle = 'White';
            context.fillRect(block.xPosition - (thickness), block.yPosition - (thickness), block.width + (thickness * 2), block.height + (thickness * 2));
        }
    }
    keyHandle(keyPressed, canvas) {
        if (this.livePiece !== undefined && this.VALID_KEYS.indexOf(keyPressed.toLowerCase()) != -1) {
            this.livePiece.movementHandler(keyPressed.toLowerCase(), canvas, this.boardLogic); //although movementHandler is not overriden in TBlock its still a member
            if (this.livePiece.hitBottom) {
                this.boardLogic.addPieceToBoard(this.livePiece);
                this.boardLogic.handleRowCleanup();
                this.livePiece = undefined;
                return;
            }
        }
    }
    update() {
        if (this.livePiece === undefined) {
            let piece;
            if (!this.firstPieceSelected) {
                piece = this.getPiece();
            }
            else {
                piece = this.nextPiece;
            }
            this.livePiece = piece;
            this.firstPieceSelected = true;
            if (this.firstPieceSelected) {
                this.nextPiece = this.getPiece();
            }
        }
        else {
            if (!this.livePiece.hitBottom) {
                this.livePiece.updateBoardPosition();
            }
            if (this.livePiece.hitBottom) {
                this.boardLogic.addPieceToBoard(this.livePiece);
                this.boardLogic.handleRowCleanup();
                this.livePiece = undefined;
                return;
            }
        }
    }
    getPiece() {
        let piece = { 1: LLPiece, 2: RLPiece, 3: SquarePiece, 4: RightZPiece, 5: LeftZPiece, 6: TBlock, 7: StickPiece };
        if (this.lastTwoPieces.length == 0) {
            const RANDOM_NUM = Math.floor(Math.random() * 7) + 1;
            this.lastTwoPieces.push(RANDOM_NUM);
            return new piece[RANDOM_NUM](GameCanvas.TILE_WIDTH, GameCanvas.TILE_HEIGHT, GameCanvas.BOARD_HEIGHT, this);
        }
        let RANDOM_NUM;
        //Getting unique piece not in the list already, so pieces arent repeat
        do {
            RANDOM_NUM = Math.floor(Math.random() * 7) + 1;
        } while (this.lastTwoPieces[0] == RANDOM_NUM || this.lastTwoPieces[1] == RANDOM_NUM);
        //if we only have one piece in the list (this occurs after the first piece, then just add another), else remove one of the pieces and append the other
        if (this.lastTwoPieces.length == 1) {
            this.lastTwoPieces.push(RANDOM_NUM);
            return new piece[RANDOM_NUM](GameCanvas.TILE_WIDTH, GameCanvas.TILE_HEIGHT, GameCanvas.BOARD_HEIGHT, this);
        }
        else {
            this.lastTwoPieces.push(RANDOM_NUM);
            this.lastTwoPieces.shift();
            return new piece[RANDOM_NUM](GameCanvas.TILE_WIDTH, GameCanvas.TILE_HEIGHT, GameCanvas.BOARD_HEIGHT, this);
        }
    }
}
GameCanvas.BOARD_WIDTH = 250;
GameCanvas.BOARD_HEIGHT = 400;
GameCanvas.TILE_WIDTH = GameCanvas.BOARD_WIDTH / 10;
GameCanvas.TILE_HEIGHT = GameCanvas.BOARD_HEIGHT / 16;
