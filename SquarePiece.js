import { PieceAbstraction } from "./PieceAbstraction";
export class SquarePiece extends PieceAbstraction {
    constructor(TILE_WIDTH, TILE_HEIGHT, BOARD_HEIGHT, canvas) {
        super();
        this.TILE_WIDTH = TILE_WIDTH;
        this.TILE_HEIGHT = TILE_HEIGHT;
        this.BOARD_HEIGHT = BOARD_HEIGHT;
        this.canvas = canvas;
        this.COLOR = "Orange";
        this.topRightBlock = {
            previewY: 25,
            previewX: 50,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 5,
            yPosition: 0,
            width: this.TILE_WIDTH,
            height: this.TILE_HEIGHT,
            color: this.COLOR
        };
        this.topLeftBlock = {
            previewY: 25,
            previewX: 25,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 4,
            yPosition: 0,
            width: this.TILE_WIDTH,
            height: this.TILE_HEIGHT,
            color: this.COLOR
        };
        this.bottomLeftBlock = {
            previewY: 50,
            previewX: 25,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 4,
            yPosition: this.TILE_HEIGHT,
            width: this.TILE_WIDTH,
            height: this.TILE_HEIGHT,
            color: this.COLOR
        };
        this.bottomRightBlock = {
            previewY: 50,
            previewX: 50,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 5,
            yPosition: this.TILE_HEIGHT,
            width: this.TILE_WIDTH,
            height: this.TILE_HEIGHT,
            color: this.COLOR
        };
        this.pieceConstructed = { amountOfBlocks: 4, color: this.COLOR, blocksContainer: [this.topRightBlock, this.topLeftBlock, this.bottomLeftBlock, this.bottomRightBlock] };
        this.pieceInitalized = true;
        this.pieceRotationProperties = { isRotatable: false, axisOfRotation: this.bottomRightBlock };
    }
    updateBoardPosition() {
        if (this.pieceInitalized) {
            super.downMovement(this.canvas);
        }
    }
    getPiece() {
        return this.pieceConstructed;
    }
}
