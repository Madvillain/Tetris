// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"PieceAbstraction.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PieceAbstraction = void 0;

var _GameCanvas = require("./GameCanvas");

//Why abstract: because i want my pieces to inherit but i dont want this class itself to have an instance
var PieceAbstraction =
/** @class */
function () {
  function PieceAbstraction() {
    this.pieceInitalized = false;
    this.hitBottom = false;
  }
  /**Movement will be written here just pass blocks container and call super.leftMovement */


  PieceAbstraction.prototype.movementHandler = function (keyPress, pieceConstructed) {
    switch (keyPress) {
      case "a":
        {
          this.leftMovement(pieceConstructed);
          break;
        }

      case "s":
        {
          this.downMovement(pieceConstructed);
          break;
        }

      case "d":
        {
          this.rightMovement(pieceConstructed);
          break;
        }

      case "e":
        {
          this.rotatePiece(pieceConstructed);
          break;
        }

      default:
        break;
    }
  }; //TODO : INTERACT WITH BOARDMATRIX


  PieceAbstraction.prototype.leftMovement = function (piece) {
    for (var i = 0; i < this.pieceConstructed.blocksContainer.length; i++) {
      this.pieceConstructed.blocksContainer[i].xPosition -= _GameCanvas.GameCanvas.TILE_HEIGHT;
    }
  };

  PieceAbstraction.prototype.rightMovement = function (piece) {
    for (var i = 0; i < this.pieceConstructed.blocksContainer.length; i++) {
      this.pieceConstructed.blocksContainer[i].xPosition += _GameCanvas.GameCanvas.TILE_HEIGHT;
    }
  };

  PieceAbstraction.prototype.downMovement = function (piece) {
    for (var i = 0; i < this.pieceConstructed.blocksContainer.length; i++) {//this.pieceConstructed.blocksContainer[i].yPosition += GameCanvas.TILE_HEIGHT;
    }
  };

  PieceAbstraction.prototype.rotatePiece = function (piece) {};

  return PieceAbstraction;
}();

exports.PieceAbstraction = PieceAbstraction;
},{"./GameCanvas":"GameCanvas.js"}],"t-block.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TBlock = void 0;

var _PieceAbstraction = require("./PieceAbstraction");

var __extends = void 0 && (void 0).__extends || function () {
  var _extendStatics = function extendStatics(d, b) {
    _extendStatics = Object.setPrototypeOf || {
      __proto__: []
    } instanceof Array && function (d, b) {
      d.__proto__ = b;
    } || function (d, b) {
      for (var p in b) {
        if (b.hasOwnProperty(p)) d[p] = b[p];
      }
    };

    return _extendStatics(d, b);
  };

  return function (d, b) {
    _extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
}();

//I did this so if the order of blocksContainer in pieceConstructed changes nothing will break
var TBlockContainer;

(function (TBlockContainer) {
  TBlockContainer[TBlockContainer["TOP_BLOCK"] = 0] = "TOP_BLOCK";
  TBlockContainer[TBlockContainer["BTM_LEFT_BLOCK"] = 1] = "BTM_LEFT_BLOCK";
  TBlockContainer[TBlockContainer["BTM_CENTER_BLOCK"] = 2] = "BTM_CENTER_BLOCK";
  TBlockContainer[TBlockContainer["BTM_RIGHT_BLOCK"] = 3] = "BTM_RIGHT_BLOCK";
})(TBlockContainer || (TBlockContainer = {}));

var TBlock =
/** @class */
function (_super) {
  __extends(TBlock, _super);

  function TBlock(TILE_WIDTH, TILE_HEIGHT, BOARD_HEIGHT) {
    var _this = _super.call(this) || this;

    _this.TILE_WIDTH = TILE_WIDTH;
    _this.TILE_HEIGHT = TILE_HEIGHT;
    _this.BOARD_HEIGHT = BOARD_HEIGHT;
    _this.pieceInitalized = false;
    _this.hitBottom = false; //Default block positions on construction

    _this.topBlock = {
      xPosition: _this.TILE_WIDTH,
      yPosition: 0,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH
    };
    _this.btmLeftBlock = {
      xPosition: 0,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH
    };
    _this.btmCenterBlock = {
      xPosition: _this.TILE_HEIGHT,
      yPosition: _this.TILE_WIDTH,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH
    };
    _this.btmRightBlock = {
      xPosition: _this.TILE_WIDTH * 2,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH
    };
    _this.pieceConstructed = {
      amountOfBlocks: 4,
      color: "Black",
      blocksContainer: [_this.topBlock, _this.btmLeftBlock, _this.btmCenterBlock, _this.btmRightBlock]
    };
    _this.pieceInitalized = true;
    return _this;
  }

  TBlock.prototype.updateBoardPosition = function (keyPress) {
    //if keypress is 'a' call super.left, is keypress is 'd' call super.right etc
    var pressed = false; //need to create rotated structures and pass a reference of them to movementHandler as an optional

    if (keyPress) {
      if (keyPress != "s") pressed = true;

      _super.prototype.movementHandler.call(this, keyPress, this.pieceConstructed);
    } //vertical change


    if (this.pieceInitalized && !pressed) {
      for (var i = 0; i < this.pieceConstructed.blocksContainer.length; i++) {
        this.pieceConstructed.blocksContainer[i].yPosition += this.TILE_HEIGHT;
      }
    } //TODO
    //check if the tetris piece as a whole has hit the floor, need to redo this later according to the BoardMeta[][]
    //add OR conditional to check if it has touched the top of another piece
    //Might need an array of the board where current positions are
    //Write pieceToBoardPosition function


    if (this.pieceConstructed.blocksContainer[2].yPosition == this.BOARD_HEIGHT - this.TILE_HEIGHT) {
      this.hitBottom = true;
    }

    this.topBlock = {
      xPosition: this.pieceConstructed.blocksContainer[TBlockContainer.TOP_BLOCK].xPosition,
      yPosition: this.pieceConstructed.blocksContainer[TBlockContainer.TOP_BLOCK].yPosition,
      width: this.TILE_HEIGHT,
      height: this.TILE_WIDTH
    };
    this.btmLeftBlock = {
      xPosition: this.pieceConstructed.blocksContainer[TBlockContainer.BTM_LEFT_BLOCK].xPosition,
      yPosition: this.pieceConstructed.blocksContainer[TBlockContainer.BTM_LEFT_BLOCK].yPosition,
      width: this.TILE_HEIGHT,
      height: this.TILE_WIDTH
    };
    this.btmCenterBlock = {
      xPosition: this.pieceConstructed.blocksContainer[TBlockContainer.BTM_CENTER_BLOCK].xPosition,
      yPosition: this.pieceConstructed.blocksContainer[TBlockContainer.BTM_CENTER_BLOCK].yPosition,
      width: this.TILE_HEIGHT,
      height: this.TILE_WIDTH
    };
    this.btmRightBlock = {
      xPosition: this.pieceConstructed.blocksContainer[TBlockContainer.BTM_RIGHT_BLOCK].xPosition,
      yPosition: this.pieceConstructed.blocksContainer[TBlockContainer.BTM_RIGHT_BLOCK].yPosition,
      width: this.TILE_HEIGHT,
      height: this.TILE_WIDTH
    }; //update blocksContainer

    this.pieceConstructed.blocksContainer = [this.topBlock, this.btmLeftBlock, this.btmCenterBlock, this.btmRightBlock];
  };

  TBlock.prototype.getPiece = function () {
    return this;
  };

  return TBlock;
}(_PieceAbstraction.PieceAbstraction);

exports.TBlock = TBlock;
},{"./PieceAbstraction":"PieceAbstraction.js"}],"GameCanvas.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GameCanvas = void 0;

var _tBlock = require("./t-block");

var __awaiter = void 0 && (void 0).__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = void 0 && (void 0).__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

var GameCanvas =
/** @class */
function () {
  function GameCanvas(canvas) {
    this.canvas = canvas;
    this.VALID_KEYS = ['w', 'a', 's', 'd', 'e'];
    this.livePiece = undefined;
    this.canvas.height = GameCanvas.BOARD_HEIGHT;
    this.canvas.width = GameCanvas.BOARD_WIDTH;
    this.drawBoard();
  } //Draws board with tiles


  GameCanvas.prototype.drawBoard = function () {
    var _a;

    var context = this.canvas.getContext("2d");
    context.clearRect(0, 0, GameCanvas.BOARD_WIDTH, GameCanvas.BOARD_HEIGHT);
    var squareWidth = GameCanvas.BOARD_WIDTH / 10;
    var sqaureHeight = GameCanvas.BOARD_HEIGHT / 16;
    var yPosition = 0;

    for (var i = 0; i < 16; i++) {
      var xPosition = 0;

      for (var j = 0; j < 10; j++) {
        context.strokeStyle = "Black";
        context.strokeRect(xPosition, yPosition, squareWidth, sqaureHeight);

        if (xPosition <= GameCanvas.BOARD_WIDTH - 25) {
          xPosition += squareWidth;
        } else {
          //we've drawn the whole row so now move onto next
          break;
        }
      }

      yPosition += sqaureHeight;
    } //if there are pieces coming down


    if (this.livePiece !== undefined) {
      for (var i = 0; i < this.livePiece.pieceConstructed.amountOfBlocks; i++) {
        drawBorder(context, this.livePiece.pieceConstructed, i);
        context.fillStyle = (_a = this.livePiece.pieceConstructed.color) !== null && _a !== void 0 ? _a : "Black";
        context.fillRect(this.livePiece.pieceConstructed.blocksContainer[i].xPosition, this.livePiece.pieceConstructed.blocksContainer[i].yPosition, this.livePiece.pieceConstructed.blocksContainer[i].width, this.livePiece.pieceConstructed.blocksContainer[i].height);
      }

      if (this.livePiece.hitBottom) {
        this.livePiece = undefined; //TODO
        //add to permenant position with boardMatrix
        //then find new piece
      }
    } //TODO
    //draw pieces that already exist on the board


    function drawBorder(context, currentPiece, i, thickness) {
      if (thickness === void 0) {
        thickness = 1;
      }

      context.fillStyle = 'White';
      context.fillRect(currentPiece.blocksContainer[i].xPosition - thickness, currentPiece.blocksContainer[i].yPosition - thickness, currentPiece.blocksContainer[i].width + thickness * 2, currentPiece.blocksContainer[i].height + thickness * 2);
    }
  };
  /* SPAMMING ANY KEY
  UPDATE GETS CALLED
  PAYLOAD ADDED
  UPDATE CALLED AGAIN AND THIS TIME NOTHING WAS PRESSED IN THAT INTERMITTENT TIME, SO IT'LL USE THE PAYLOAD INFORMATION TO MOVE THE PIECE LEFT OR RIGHT
  SINCE IM SPAMMING AFTER THAT INTERMITTENT UPDATE CALL WHERE THE PIECE MOVED, I'LL GET ANOTHER ASYNC CALL
  */
  //SYNC CODE HAS TO SAY, EVERY X SECONDS THE PIECE HAS TO FALL REGARDLESS
  //ASYNC CODE HAS TO SAY, IF KEY PRESSED MOVE IT AROUND IF POSSIBLE


  GameCanvas.prototype.asyncUpdate = function () {
    if (GameCanvas.payload !== null && GameCanvas.payload.pressed && this.VALID_KEYS.indexOf(GameCanvas.payload.key.toLowerCase()) != -1) {
      this.livePiece = this.livePiece.getPiece();
      this.livePiece.updateBoardPosition(GameCanvas.payload.key);
      GameCanvas.payload = null;
    }

    var readKey = function readKey() {
      return new Promise(function (resolve) {
        return window.addEventListener('keypress', resolve, {
          once: true
        });
      });
    };

    (function () {
      return __awaiter(this, void 0, void 0, function () {
        var x, payload;
        return __generator(this, function (_a) {
          switch (_a.label) {
            case 0:
              return [4
              /*yield*/
              , readKey()];

            case 1:
              x = _a.sent();
              console.log('Pressed', String.fromCharCode(x.which));

              if (GameCanvas.payload == null && x) {
                payload = {
                  pressed: true,
                  key: String.fromCharCode(x.which)
                };
                GameCanvas.payload = payload;
              }

              return [2
              /*return*/
              ];
          }
        });
      });
    })();
  };

  GameCanvas.prototype.update = function () {
    if (this.livePiece === undefined) {
      var piece = new _tBlock.TBlock(GameCanvas.TILE_WIDTH, GameCanvas.TILE_HEIGHT, GameCanvas.BOARD_HEIGHT);
    } else {
      this.livePiece = this.livePiece.getPiece();
      this.livePiece.updateBoardPosition();
    }
  };

  GameCanvas.BOARD_WIDTH = 250;
  GameCanvas.BOARD_HEIGHT = 400;
  GameCanvas.TILE_WIDTH = GameCanvas.BOARD_WIDTH / 10;
  GameCanvas.TILE_HEIGHT = GameCanvas.BOARD_HEIGHT / 16;
  GameCanvas.payload = null;
  return GameCanvas;
}();

exports.GameCanvas = GameCanvas;
},{"./t-block":"t-block.js"}],"Main.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.board = exports.MainLoop = void 0;

var _GameCanvas = require("./GameCanvas");

var MainLoop =
/** @class */
function () {
  function MainLoop() {}

  MainLoop.mainLoop = function (timestamp) {
    if (timestamp < MainLoop.lastFrameTimeMs + 1000 / MainLoop.maxFPS) {
      requestAnimationFrame(MainLoop.mainLoop);
      return;
    }

    MainLoop.lastFrameTimeMs = timestamp;
    board.update();
    board.drawBoard();
    requestAnimationFrame(MainLoop.mainLoop);
  };

  MainLoop.lastFrameTimeMs = 100;
  MainLoop.maxFPS = 1.5; // The last time the loop was run, max fps

  MainLoop.fpsLoopCounter = 0;
  MainLoop.pieceMovedOnClick = false;
  return MainLoop;
}();

exports.MainLoop = MainLoop;
var canvas = document.getElementById('canvas'); //ge the canvas from HTML document

var board = new _GameCanvas.GameCanvas(canvas);
exports.board = board;
requestAnimationFrame(MainLoop.mainLoop);
},{"./GameCanvas":"GameCanvas.js"}],"asyncMain.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Loop = void 0;

var _Main = require("./Main");

var Loop =
/** @class */
function () {
  function Loop() {}

  Loop.mainLoop = function (timestamp) {
    if (timestamp < Loop.lastFrameTimeMs + 1000 / Loop.maxFPS) {
      requestAnimationFrame(Loop.mainLoop);
      return;
    }

    Loop.lastFrameTimeMs = timestamp;

    _Main.board.asyncUpdate();

    _Main.board.drawBoard();

    requestAnimationFrame(Loop.mainLoop);
  };

  Loop.lastFrameTimeMs = 100;
  Loop.maxFPS = 1.5; // The last time the loop was run, max fps

  Loop.fpsLoopCounter = 0;
  Loop.pieceMovedOnClick = false;
  return Loop;
}();

exports.Loop = Loop;
requestAnimationFrame(Loop.mainLoop);
},{"./Main":"Main.js"}],"../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "45649" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","asyncMain.js"], null)
//# sourceMappingURL=/asyncMain.96354cb5.js.map