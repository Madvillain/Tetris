// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"PieceAbstraction.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PieceAbstraction = void 0;

var _GameCanvas = require("./GameCanvas");

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

//Why abstract: because i want my pieces to inherit but i dont want this class itself to have an instance
var PieceAbstraction = /*#__PURE__*/function () {
  function PieceAbstraction() {
    _classCallCheck(this, PieceAbstraction);

    this.pieceInitalized = false;
    this.hitBottom = false;
  }
  /**Movement will be written here just pass blocks container and call super.leftMovement */


  _createClass(PieceAbstraction, [{
    key: "movementHandler",
    value: function movementHandler(keyPress, canvas, boardLogic) {
      switch (keyPress) {
        case "a":
          {
            this.leftMovement(canvas);
            break;
          }

        case "s":
          {
            this.downMovement(canvas);
            break;
          }

        case "d":
          {
            this.rightMovement(canvas);
            break;
          }

        case "w":
          {
            this.rotatePiece(canvas, boardLogic);
            break;
          }

        default:
          break;
      }
    } //These non-abstract methods are members of the implementer since they are inherited. So when we call `this` we are referencing the caller class. So we can access abstract properties from the caller

  }, {
    key: "leftMovement",
    value: function leftMovement(canvas) {
      //Canvas Bounds Check
      var _iterator = _createForOfIteratorHelper(this.pieceConstructed.blocksContainer),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var block = _step.value;

          if (block.xPosition - _GameCanvas.GameCanvas.TILE_WIDTH < canvas.LEFT_MARGIN) {
            return;
          }
        } //Checking collision with another piece on the board to the left

      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      var _iterator2 = _createForOfIteratorHelper(this.pieceConstructed.blocksContainer),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var _block = _step2.value;
          var blockRowIndex = canvas.boardLogic.getMatrixY(_block.yPosition);
          var blockTileIndex = canvas.boardLogic.getMatrixY(_block.xPosition);

          if (canvas.boardLogic.BoardMatrix[blockRowIndex][blockTileIndex - 1] !== undefined) {
            return;
          }
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }

      for (var i = 0; i < this.pieceConstructed.blocksContainer.length; i++) {
        this.pieceConstructed.blocksContainer[i].xPosition -= _GameCanvas.GameCanvas.TILE_HEIGHT;
        canvas.drawBoard();
      }
    }
  }, {
    key: "rightMovement",
    value: function rightMovement(canvas) {
      //Canvas Bounds Check
      var _iterator3 = _createForOfIteratorHelper(this.pieceConstructed.blocksContainer),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var block = _step3.value;

          if (block.xPosition + _GameCanvas.GameCanvas.TILE_WIDTH > canvas.RIGHT_MARGIN) {
            return;
          }
        } //Checking collision with another piece on the board to the left

      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }

      var _iterator4 = _createForOfIteratorHelper(this.pieceConstructed.blocksContainer),
          _step4;

      try {
        for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
          var _block2 = _step4.value;
          var blockRowIndex = canvas.boardLogic.getMatrixY(_block2.yPosition);
          var blockTileIndex = canvas.boardLogic.getMatrixY(_block2.xPosition); //This works because the piece isnt on the board yet

          if (canvas.boardLogic.BoardMatrix[blockRowIndex][blockTileIndex + 1] !== undefined) {
            return;
          }
        }
      } catch (err) {
        _iterator4.e(err);
      } finally {
        _iterator4.f();
      }

      for (var i = 0; i < this.pieceConstructed.blocksContainer.length; i++) {
        this.pieceConstructed.blocksContainer[i].xPosition += _GameCanvas.GameCanvas.TILE_HEIGHT;
        canvas.drawBoard();
      }
    }
  }, {
    key: "downMovement",
    value: function downMovement(canvas) {
      //Canvas Bounds Check
      var _iterator5 = _createForOfIteratorHelper(this.pieceConstructed.blocksContainer),
          _step5;

      try {
        for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
          var block = _step5.value;

          if (block.yPosition + _GameCanvas.GameCanvas.TILE_HEIGHT > canvas.BOTTOM_MARGIN) {
            this.hitBottom = true;
            return;
          }
        }
      } catch (err) {
        _iterator5.e(err);
      } finally {
        _iterator5.f();
      }

      var _iterator6 = _createForOfIteratorHelper(this.pieceConstructed.blocksContainer),
          _step6;

      try {
        for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
          var _block3 = _step6.value;
          var blockRowIndex = canvas.boardLogic.getMatrixY(_block3.yPosition);
          var blockTileIndex = canvas.boardLogic.getMatrixY(_block3.xPosition);

          if (canvas.boardLogic.BoardMatrix[blockRowIndex + 1][blockTileIndex] !== undefined) {
            this.hitBottom = true;
            return;
          }
        }
      } catch (err) {
        _iterator6.e(err);
      } finally {
        _iterator6.f();
      }

      var _iterator7 = _createForOfIteratorHelper(this.pieceConstructed.blocksContainer),
          _step7;

      try {
        for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
          var _block4 = _step7.value;
          _block4.yPosition += _GameCanvas.GameCanvas.TILE_HEIGHT;
          canvas.drawBoard();
        }
      } catch (err) {
        _iterator7.e(err);
      } finally {
        _iterator7.f();
      }
    }
  }, {
    key: "rotatePiece",
    value: function rotatePiece(canvas, boardLogic) {
      //Square is -1 because no point in rotating it
      if (this.hitBottom) {
        return;
      }

      if (!this.pieceRotationProperties.isRotatable) {
        return;
      } //Making a copy just in case not all pieces are valid rotations


      var originalPiece = {
        amountOfBlocks: this.pieceConstructed.amountOfBlocks,
        blocksContainer: this.pieceConstructed.blocksContainer,
        speed: this.pieceConstructed.speed,
        color: this.pieceConstructed.color
      };

      var _iterator8 = _createForOfIteratorHelper(this.pieceConstructed.blocksContainer),
          _step8;

      try {
        for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
          var block = _step8.value;
          var blockVector = boardLogic.vectorizePoint(block);
          var axisOfRotationBlockVector = boardLogic.vectorizePoint(this.pieceRotationProperties.axisOfRotation);
          var newVectorY = blockVector[0] - axisOfRotationBlockVector[0];
          var newVectorX = blockVector[1] - axisOfRotationBlockVector[1];
          var newVector = [newVectorY, newVectorX];
          var dotProduct = this.rotateBy90(newVector);
          var finalVector = [axisOfRotationBlockVector[0] + dotProduct[0], axisOfRotationBlockVector[1] + dotProduct[1]]; //Have to check if new piece in bounds and doesnt collide

          if (!this.matrixSpaceAvailable(finalVector, boardLogic)) {
            this.pieceConstructed = originalPiece;
            return;
          } //Set new Coordinates from Vector


          var canvasY = finalVector[0] * _GameCanvas.GameCanvas.TILE_HEIGHT;
          var canvasX = finalVector[1] * _GameCanvas.GameCanvas.TILE_WIDTH;

          if (canvasY > canvas.BOTTOM_MARGIN || canvasX < 0 || canvasX > canvas.RIGHT_MARGIN) {
            this.pieceConstructed = originalPiece;
            return;
          }

          block.xPosition = canvasX;
          block.yPosition = canvasY;
        }
      } catch (err) {
        _iterator8.e(err);
      } finally {
        _iterator8.f();
      }

      canvas.drawBoard();
    }
  }, {
    key: "rotateBy90",
    value: function rotateBy90(vector) {
      return [vector[0] * 0 + vector[1] * -1, vector[0] * 1 + vector[1] * 0];
    }
  }, {
    key: "matrixSpaceAvailable",
    value: function matrixSpaceAvailable(matrixSpace, boardLogic) {
      var y = matrixSpace[0];
      var x = matrixSpace[1];

      if (boardLogic.getMatrix()[y][x] !== undefined) {
        return false;
      }

      return true;
    }
  }]);

  return PieceAbstraction;
}();

exports.PieceAbstraction = PieceAbstraction;
},{"./GameCanvas":"GameCanvas.js"}],"LeftLPiece.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LLPiece = void 0;

var _PieceAbstraction2 = require("./PieceAbstraction");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var LLPiece = /*#__PURE__*/function (_PieceAbstraction) {
  _inherits(LLPiece, _PieceAbstraction);

  var _super = _createSuper(LLPiece);

  function LLPiece(TILE_WIDTH, TILE_HEIGHT, BOARD_HEIGHT, canvas) {
    var _this;

    _classCallCheck(this, LLPiece);

    _this = _super.call(this);
    _this.TILE_WIDTH = TILE_WIDTH;
    _this.TILE_HEIGHT = TILE_HEIGHT;
    _this.BOARD_HEIGHT = BOARD_HEIGHT;
    _this.canvas = canvas;
    _this.COLOR = "Black"; //Default block positions on construction

    _this.topBlock = {
      previewY: 25,
      previewX: 25,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 3,
      yPosition: 0,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH,
      color: _this.COLOR
    };
    _this.btmLeftBlock = {
      previewY: 50,
      previewX: 25,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 3,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH,
      color: _this.COLOR
    };
    _this.btmCenterBlock = {
      previewY: 50,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 4,
      yPosition: _this.TILE_WIDTH,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH,
      color: _this.COLOR
    };
    _this.btmRightBlock = {
      previewY: 50,
      previewX: 75,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 5,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH,
      color: _this.COLOR
    };
    _this.pieceConstructed = {
      amountOfBlocks: 4,
      color: _this.COLOR,
      blocksContainer: [_this.topBlock, _this.btmLeftBlock, _this.btmCenterBlock, _this.btmRightBlock]
    };
    _this.pieceInitalized = true;
    _this.pieceRotationProperties = {
      isRotatable: true,
      axisOfRotation: _this.btmCenterBlock
    };
    return _this;
  }

  _createClass(LLPiece, [{
    key: "updateBoardPosition",
    value: function updateBoardPosition() {
      if (this.pieceInitalized) {
        _get(_getPrototypeOf(LLPiece.prototype), "downMovement", this).call(this, this.canvas); //check if it'll hit bounds

      }
    }
  }, {
    key: "getPiece",
    value: function getPiece() {
      return this.pieceConstructed;
    }
  }]);

  return LLPiece;
}(_PieceAbstraction2.PieceAbstraction);

exports.LLPiece = LLPiece;
},{"./PieceAbstraction":"PieceAbstraction.js"}],"RightLPiece.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RLPiece = void 0;

var _PieceAbstraction2 = require("./PieceAbstraction");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var RLPiece = /*#__PURE__*/function (_PieceAbstraction) {
  _inherits(RLPiece, _PieceAbstraction);

  var _super = _createSuper(RLPiece);

  function RLPiece(TILE_WIDTH, TILE_HEIGHT, BOARD_HEIGHT, canvas) {
    var _this;

    _classCallCheck(this, RLPiece);

    _this = _super.call(this);
    _this.TILE_WIDTH = TILE_WIDTH;
    _this.TILE_HEIGHT = TILE_HEIGHT;
    _this.BOARD_HEIGHT = BOARD_HEIGHT;
    _this.canvas = canvas;
    _this.COLOR = "Red"; //Default block positions on construction

    _this.topBlock = {
      previewY: 25,
      previewX: 25,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 5,
      yPosition: 0,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH,
      color: _this.COLOR
    };
    _this.btmLeftBlock = {
      previewY: 50,
      previewX: 25,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 3,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH,
      color: _this.COLOR
    };
    _this.btmCenterBlock = {
      previewY: 50,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_HEIGHT * 4,
      yPosition: _this.TILE_WIDTH,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH,
      color: _this.COLOR
    };
    _this.btmRightBlock = {
      previewY: 50,
      previewX: 75,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 5,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH,
      color: _this.COLOR
    };
    _this.pieceConstructed = {
      amountOfBlocks: 4,
      color: _this.COLOR,
      blocksContainer: [_this.topBlock, _this.btmLeftBlock, _this.btmCenterBlock, _this.btmRightBlock]
    };
    _this.pieceInitalized = true;
    _this.pieceRotationProperties = {
      isRotatable: true,
      axisOfRotation: _this.btmCenterBlock
    };
    return _this;
  }

  _createClass(RLPiece, [{
    key: "updateBoardPosition",
    value: function updateBoardPosition() {
      if (this.pieceInitalized) {
        _get(_getPrototypeOf(RLPiece.prototype), "downMovement", this).call(this, this.canvas); //check if it'll hit bounds

      }
    }
  }, {
    key: "getPiece",
    value: function getPiece() {
      return this.pieceConstructed;
    }
  }]);

  return RLPiece;
}(_PieceAbstraction2.PieceAbstraction);

exports.RLPiece = RLPiece;
},{"./PieceAbstraction":"PieceAbstraction.js"}],"SquarePiece.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SquarePiece = void 0;

var _PieceAbstraction2 = require("./PieceAbstraction");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var SquarePiece = /*#__PURE__*/function (_PieceAbstraction) {
  _inherits(SquarePiece, _PieceAbstraction);

  var _super = _createSuper(SquarePiece);

  function SquarePiece(TILE_WIDTH, TILE_HEIGHT, BOARD_HEIGHT, canvas) {
    var _this;

    _classCallCheck(this, SquarePiece);

    _this = _super.call(this);
    _this.TILE_WIDTH = TILE_WIDTH;
    _this.TILE_HEIGHT = TILE_HEIGHT;
    _this.BOARD_HEIGHT = BOARD_HEIGHT;
    _this.canvas = canvas;
    _this.COLOR = "Orange";
    _this.topRightBlock = {
      previewY: 25,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 5,
      yPosition: 0,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.topLeftBlock = {
      previewY: 25,
      previewX: 25,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 4,
      yPosition: 0,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.bottomLeftBlock = {
      previewY: 50,
      previewX: 25,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 4,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.bottomRightBlock = {
      previewY: 50,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 5,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.pieceConstructed = {
      amountOfBlocks: 4,
      color: _this.COLOR,
      blocksContainer: [_this.topRightBlock, _this.topLeftBlock, _this.bottomLeftBlock, _this.bottomRightBlock]
    };
    _this.pieceInitalized = true;
    _this.pieceRotationProperties = {
      isRotatable: false,
      axisOfRotation: _this.bottomRightBlock
    };
    return _this;
  }

  _createClass(SquarePiece, [{
    key: "updateBoardPosition",
    value: function updateBoardPosition() {
      if (this.pieceInitalized) {
        _get(_getPrototypeOf(SquarePiece.prototype), "downMovement", this).call(this, this.canvas);
      }
    }
  }, {
    key: "getPiece",
    value: function getPiece() {
      return this.pieceConstructed;
    }
  }]);

  return SquarePiece;
}(_PieceAbstraction2.PieceAbstraction);

exports.SquarePiece = SquarePiece;
},{"./PieceAbstraction":"PieceAbstraction.js"}],"RightZPiece.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RightZPiece = void 0;

var _PieceAbstraction2 = require("./PieceAbstraction");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var RightZPiece = /*#__PURE__*/function (_PieceAbstraction) {
  _inherits(RightZPiece, _PieceAbstraction);

  var _super = _createSuper(RightZPiece);

  function RightZPiece(TILE_WIDTH, TILE_HEIGHT, BOARD_HEIGHT, canvas) {
    var _this;

    _classCallCheck(this, RightZPiece);

    _this = _super.call(this);
    _this.TILE_WIDTH = TILE_WIDTH;
    _this.TILE_HEIGHT = TILE_HEIGHT;
    _this.BOARD_HEIGHT = BOARD_HEIGHT;
    _this.canvas = canvas;
    _this.COLOR = "Blue";
    _this.topRightBlock = {
      previewY: 25,
      previewX: 75,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 6,
      yPosition: 0,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.topMiddleBlock = {
      previewY: 25,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 5,
      yPosition: 0,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.bottomMiddleBlock = {
      previewY: 50,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 5,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.bottomLeftBlock = {
      previewY: 50,
      previewX: 25,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 4,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.pieceConstructed = {
      amountOfBlocks: 4,
      color: _this.COLOR,
      blocksContainer: [_this.topRightBlock, _this.topMiddleBlock, _this.bottomMiddleBlock, _this.bottomLeftBlock]
    };
    _this.pieceRotationProperties = {
      isRotatable: true,
      axisOfRotation: _this.bottomMiddleBlock
    };
    _this.pieceInitalized = true;
    return _this;
  }

  _createClass(RightZPiece, [{
    key: "updateBoardPosition",
    value: function updateBoardPosition() {
      if (this.pieceInitalized) {
        _get(_getPrototypeOf(RightZPiece.prototype), "downMovement", this).call(this, this.canvas);
      }
    }
  }, {
    key: "getPiece",
    value: function getPiece() {
      return this.pieceConstructed;
    }
  }]);

  return RightZPiece;
}(_PieceAbstraction2.PieceAbstraction);

exports.RightZPiece = RightZPiece;
},{"./PieceAbstraction":"PieceAbstraction.js"}],"LeftZPiece.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LeftZPiece = void 0;

var _PieceAbstraction2 = require("./PieceAbstraction");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var LeftZPiece = /*#__PURE__*/function (_PieceAbstraction) {
  _inherits(LeftZPiece, _PieceAbstraction);

  var _super = _createSuper(LeftZPiece);

  function LeftZPiece(TILE_WIDTH, TILE_HEIGHT, BOARD_HEIGHT, canvas) {
    var _this;

    _classCallCheck(this, LeftZPiece);

    _this = _super.call(this);
    _this.TILE_WIDTH = TILE_WIDTH;
    _this.TILE_HEIGHT = TILE_HEIGHT;
    _this.BOARD_HEIGHT = BOARD_HEIGHT;
    _this.canvas = canvas;
    _this.COLOR = "GREEN";
    _this.topLeftBlock = {
      previewY: 25,
      previewX: 25,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 3,
      yPosition: 0,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.topMiddleBlock = {
      previewY: 25,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 4,
      yPosition: 0,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.bottomMiddleBlock = {
      previewY: 50,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 4,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.bottomRightBlock = {
      previewY: 50,
      previewX: 75,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 5,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.pieceConstructed = {
      amountOfBlocks: 4,
      color: _this.COLOR,
      blocksContainer: [_this.topLeftBlock, _this.topMiddleBlock, _this.bottomMiddleBlock, _this.bottomRightBlock]
    };
    _this.pieceRotationProperties = {
      isRotatable: true,
      axisOfRotation: _this.bottomMiddleBlock
    };
    _this.pieceInitalized = true;
    return _this;
  }

  _createClass(LeftZPiece, [{
    key: "updateBoardPosition",
    value: function updateBoardPosition() {
      if (this.pieceInitalized) {
        _get(_getPrototypeOf(LeftZPiece.prototype), "downMovement", this).call(this, this.canvas); //check if it'll hit bounds

      }
    }
  }, {
    key: "getPiece",
    value: function getPiece() {
      return this.pieceConstructed;
    }
  }]);

  return LeftZPiece;
}(_PieceAbstraction2.PieceAbstraction);

exports.LeftZPiece = LeftZPiece;
},{"./PieceAbstraction":"PieceAbstraction.js"}],"StickPiece.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StickPiece = void 0;

var _PieceAbstraction2 = require("./PieceAbstraction");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var StickPiece = /*#__PURE__*/function (_PieceAbstraction) {
  _inherits(StickPiece, _PieceAbstraction);

  var _super = _createSuper(StickPiece);

  function StickPiece(TILE_WIDTH, TILE_HEIGHT, BOARD_HEIGHT, canvas) {
    var _this;

    _classCallCheck(this, StickPiece);

    _this = _super.call(this);
    _this.TILE_WIDTH = TILE_WIDTH;
    _this.TILE_HEIGHT = TILE_HEIGHT;
    _this.BOARD_HEIGHT = BOARD_HEIGHT;
    _this.canvas = canvas;
    _this.COLOR = "Teal";
    _this.topBlock = {
      previewY: 25,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 4,
      yPosition: 0,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.secondBlock = {
      previewY: 50,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 4,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.middleBlock = {
      previewY: 75,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 4,
      yPosition: _this.TILE_HEIGHT * 2,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.bottomBlock = {
      previewY: 100,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 4,
      yPosition: _this.TILE_HEIGHT * 3,
      width: _this.TILE_WIDTH,
      height: _this.TILE_HEIGHT,
      color: _this.COLOR
    };
    _this.pieceConstructed = {
      amountOfBlocks: 4,
      color: _this.COLOR,
      blocksContainer: [_this.topBlock, _this.secondBlock, _this.middleBlock, _this.bottomBlock]
    };
    _this.pieceRotationProperties = {
      isRotatable: true,
      axisOfRotation: _this.middleBlock
    };
    _this.pieceInitalized = true;
    return _this;
  }

  _createClass(StickPiece, [{
    key: "updateBoardPosition",
    value: function updateBoardPosition() {
      if (this.pieceInitalized) {
        _get(_getPrototypeOf(StickPiece.prototype), "downMovement", this).call(this, this.canvas);
      }
    }
  }, {
    key: "getPiece",
    value: function getPiece() {
      return this.pieceConstructed;
    }
  }]);

  return StickPiece;
}(_PieceAbstraction2.PieceAbstraction);

exports.StickPiece = StickPiece;
},{"./PieceAbstraction":"PieceAbstraction.js"}],"t-block.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TBlock = void 0;

var _PieceAbstraction2 = require("./PieceAbstraction");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

//this isnt really neccessary but its a nicer way to call pieceConstructed.blocksContainer array then by indicies of 0,1,2,3 especially. So make sure it's implemented in right order
var TBlock = /*#__PURE__*/function (_PieceAbstraction) {
  _inherits(TBlock, _PieceAbstraction);

  var _super = _createSuper(TBlock);

  function TBlock(TILE_WIDTH, TILE_HEIGHT, BOARD_HEIGHT, canvas) {
    var _this;

    _classCallCheck(this, TBlock);

    _this = _super.call(this);
    _this.TILE_WIDTH = TILE_WIDTH;
    _this.TILE_HEIGHT = TILE_HEIGHT;
    _this.BOARD_HEIGHT = BOARD_HEIGHT;
    _this.canvas = canvas;
    _this.pieceInitalized = false;
    _this.hitBottom = false;
    _this.COLOR = "Purple"; //Default block positions on construction

    _this.topBlock = {
      previewY: 25,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 4,
      yPosition: 0,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH,
      color: _this.COLOR
    };
    _this.btmLeftBlock = {
      previewY: 50,
      previewX: 25,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 3,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH,
      color: _this.COLOR
    };
    _this.btmCenterBlock = {
      previewY: 50,
      previewX: 50,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 4,
      yPosition: _this.TILE_WIDTH,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH,
      color: _this.COLOR
    };
    _this.btmRightBlock = {
      previewY: 50,
      previewX: 75,
      previewWidth: 25,
      previewHeight: 25,
      xPosition: _this.TILE_WIDTH * 5,
      yPosition: _this.TILE_HEIGHT,
      width: _this.TILE_HEIGHT,
      height: _this.TILE_WIDTH,
      color: _this.COLOR
    };
    _this.pieceConstructed = {
      amountOfBlocks: 4,
      color: _this.COLOR,
      blocksContainer: [_this.topBlock, _this.btmLeftBlock, _this.btmCenterBlock, _this.btmRightBlock]
    };
    _this.pieceInitalized = true;
    _this.pieceRotationProperties = {
      isRotatable: true,
      axisOfRotation: _this.btmCenterBlock
    };
    return _this;
  }

  _createClass(TBlock, [{
    key: "updateBoardPosition",
    value: function updateBoardPosition() {
      if (this.pieceInitalized) {
        _get(_getPrototypeOf(TBlock.prototype), "downMovement", this).call(this, this.canvas);
      }
    }
  }, {
    key: "getPiece",
    value: function getPiece() {
      return this.pieceConstructed;
    }
  }]);

  return TBlock;
}(_PieceAbstraction2.PieceAbstraction);

exports.TBlock = TBlock;
},{"./PieceAbstraction":"PieceAbstraction.js"}],"BoardLogicModel.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.boardLogicModel = exports.MatrixEntry = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var MatrixEntry;
exports.MatrixEntry = MatrixEntry;

(function (MatrixEntry) {
  MatrixEntry[MatrixEntry["EMPTY"] = 0] = "EMPTY";
  MatrixEntry[MatrixEntry["FILLED"] = 1] = "FILLED";
})(MatrixEntry || (exports.MatrixEntry = MatrixEntry = {})); //rather than typing MatrixEntry.EMPTY


var EMPTY = MatrixEntry.EMPTY;

var boardLogicModel = /*#__PURE__*/function () {
  function boardLogicModel(TILE_HEIGHT, TILE_WIDTH) {
    _classCallCheck(this, boardLogicModel);

    this.TILE_HEIGHT = TILE_HEIGHT;
    this.TILE_WIDTH = TILE_WIDTH; //16 rows, 10 columns

    this.BoardMatrix = [[undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined]];
  }

  _createClass(boardLogicModel, [{
    key: "addPieceToBoard",
    value: function addPieceToBoard(piece) {
      //Adding pieces to the Map && Matrix
      for (var i = 0; i < piece.pieceConstructed.blocksContainer.length; i++) {
        var matrixY = this.getMatrixY(piece.pieceConstructed.blocksContainer[i].yPosition);
        var matrixX = this.getMatrixX(piece.pieceConstructed.blocksContainer[i].xPosition);
        this.BoardMatrix[matrixY][matrixX] = piece.pieceConstructed.blocksContainer[i]; //y is row, x is column
      }
    }
  }, {
    key: "getMatrix",
    value: function getMatrix() {
      return this.BoardMatrix;
    }
  }, {
    key: "getMatrixY",
    value: function getMatrixY(canvasY) {
      return canvasY / this.TILE_HEIGHT;
    }
  }, {
    key: "getMatrixX",
    value: function getMatrixX(canvasX) {
      return canvasX / this.TILE_WIDTH;
    }
  }, {
    key: "blockMatrixInteraction",
    value: function blockMatrixInteraction(block, canMove) {
      var matrixY = this.getMatrixY(block.yPosition);
      var matrixX = this.getMatrixX(block.xPosition);
      canMove(matrixY, matrixX);
    }
  }, {
    key: "vectorizePoint",
    value: function vectorizePoint(block) {
      var matrixY = this.getMatrixY(block.yPosition);
      var matrixX = this.getMatrixX(block.xPosition);
      return [matrixY, matrixX];
    }
  }, {
    key: "handleRowCleanup",
    value: function handleRowCleanup() {
      //Go through each row in the matrix. Check if row is full
      //store the row it is in an array
      //sort that array from greatest to least (bottom to top)
      //remove all of the blocks in that array, set the whole row to undefined
      //loop through from the bottom to next highest
      //check if the row above the row we started from is full, (peak into the array)
      //if it is, then set tiledown to + this.tile_height * x (where x is the number of consecutive full arrays)
      //push the array down to currentRow + x rows (remmeber concurrent)
      //inside each row of the array, make sure you change the this.tileHeight to be aligned with the matrix
      //so if you moved it two rows down, Block.yPosition += x * this.tile_height
      var rowsFilled = []; //Here we are adding all full rows

      for (var row = 0; row < this.BoardMatrix.length; row++) {
        for (var tile = 0; tile < this.BoardMatrix[row].length; tile++) {
          if (this.BoardMatrix[row][tile] === undefined) {
            break;
          } else if (tile == this.BoardMatrix[row].length - 1) {
            rowsFilled.push(row);
          }
        }
      }

      for (var _row = 0; _row < rowsFilled.length; _row++) {
        for (var _tile = 0; _tile < this.BoardMatrix[rowsFilled[_row]].length; _tile++) {
          this.BoardMatrix[rowsFilled[_row]][_tile] = undefined;
        }
      }

      rowsFilled = rowsFilled.sort(function (a, b) {
        return b - a;
      }); //sorting the array from greatest to least (bottom to top)

      if (rowsFilled.length > 0) {
        //iterating through each row
        for (var i = 0; i < rowsFilled.length; i++) {
          for (var rowToShift = rowsFilled[i] - 1; rowToShift >= 0; rowToShift--) {
            //rowToShift == 14
            //iteration to check for consecutive rows
            var x = 1; //Checking for consecutive rows

            while (rowToShift == rowsFilled[i + x]) {
              rowToShift--;
              x++;
            }

            for (var _tile2 = 0; _tile2 < this.BoardMatrix[rowToShift].length; _tile2++) {
              if (this.BoardMatrix[rowToShift][_tile2] !== undefined) {
                var temp = this.BoardMatrix[rowToShift][_tile2]; //Temp object

                temp.yPosition += x * this.TILE_HEIGHT;
                this.BoardMatrix[rowToShift][_tile2] = undefined;
                this.BoardMatrix[rowToShift + x][_tile2] = temp;
              }
            }
          }
        }
      }
    }
  }]);

  return boardLogicModel;
}();

exports.boardLogicModel = boardLogicModel;
},{}],"GameCanvas.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GameCanvas = void 0;

var _LeftLPiece = require("./LeftLPiece");

var _RightLPiece = require("./RightLPiece");

var _SquarePiece = require("./SquarePiece");

var _RightZPiece = require("./RightZPiece");

var _LeftZPiece = require("./LeftZPiece");

var _StickPiece = require("./StickPiece");

var _tBlock = require("./t-block");

var _BoardLogicModel = require("./BoardLogicModel");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var GameCanvas = /*#__PURE__*/function () {
  function GameCanvas(canvas, previewCanvas) {
    _classCallCheck(this, GameCanvas);

    this.canvas = canvas;
    this.previewCanvas = previewCanvas;
    this.LEFT_MARGIN = 0;
    this.RIGHT_MARGIN = GameCanvas.BOARD_WIDTH - GameCanvas.TILE_WIDTH;
    this.BOTTOM_MARGIN = GameCanvas.BOARD_HEIGHT - GameCanvas.TILE_HEIGHT;
    this.VALID_KEYS = ['w', 'a', 's', 'd', 'e'];
    this.livePiece = undefined;
    this.lastTwoPieces = [];
    this.LOCK = false;
    this.lastMoveTaken = false;
    this.firstPieceSelected = false; //Main Canvas

    this.canvas.height = GameCanvas.BOARD_HEIGHT;
    this.canvas.width = GameCanvas.BOARD_WIDTH; //PreviewCanvas

    this.previewCanvas.height = 125;
    this.previewCanvas.width = 125;
    this.boardLogic = new _BoardLogicModel.boardLogicModel(GameCanvas.TILE_HEIGHT, GameCanvas.TILE_WIDTH); //initalizing boardLogicController

    this.drawBoard();
  } //Draws board with tiles


  _createClass(GameCanvas, [{
    key: "drawBoard",
    value: function drawBoard() {
      var _a, _b; //Setup Preview


      var previewContext = this.previewCanvas.getContext("2d");
      previewContext.clearRect(0, 0, 125, 125);

      if (this.nextPiece) {
        for (var i = 0; i < this.nextPiece.pieceConstructed.amountOfBlocks; i++) {
          drawBorderForPreview(previewContext, this.nextPiece.pieceConstructed.blocksContainer[i]);
          previewContext.fillStyle = (_a = this.nextPiece.pieceConstructed.color) !== null && _a !== void 0 ? _a : "Black";
          previewContext.fillRect(this.nextPiece.pieceConstructed.blocksContainer[i].previewX, this.nextPiece.pieceConstructed.blocksContainer[i].previewY, this.nextPiece.pieceConstructed.blocksContainer[i].previewWidth, this.nextPiece.pieceConstructed.blocksContainer[i].previewHeight);
        }
      } //Draw Main Board on Canvas


      var context = this.canvas.getContext("2d");
      context.clearRect(0, 0, GameCanvas.BOARD_WIDTH, GameCanvas.BOARD_HEIGHT);
      var squareWidth = GameCanvas.BOARD_WIDTH / 10;
      var sqaureHeight = GameCanvas.BOARD_HEIGHT / 16;
      var yPosition = 0;

      for (var _i = 0; _i < 16; _i++) {
        var xPosition = 0;

        for (var j = 0; j < 10; j++) {
          context.strokeStyle = "Black";
          context.strokeRect(xPosition, yPosition, squareWidth, sqaureHeight);

          if (xPosition <= GameCanvas.BOARD_WIDTH - 25) {
            xPosition += squareWidth;
          } else {
            //we've drawn the whole row so now move onto next
            break;
          }
        }

        yPosition += sqaureHeight;
      } //Draw Pieces coming down


      if (this.livePiece !== undefined) {
        for (var _i2 = 0; _i2 < this.livePiece.pieceConstructed.amountOfBlocks; _i2++) {
          drawBorder(context, this.livePiece.pieceConstructed.blocksContainer[_i2]);
          context.fillStyle = (_b = this.livePiece.pieceConstructed.color) !== null && _b !== void 0 ? _b : "Black";
          context.fillRect(this.livePiece.pieceConstructed.blocksContainer[_i2].xPosition, this.livePiece.pieceConstructed.blocksContainer[_i2].yPosition, this.livePiece.pieceConstructed.blocksContainer[_i2].width, this.livePiece.pieceConstructed.blocksContainer[_i2].height);
        }
      } //Draw blocks already set


      for (var row = 0; row < this.boardLogic.BoardMatrix.length; row++) {
        for (var tile = 0; tile < this.boardLogic.BoardMatrix[row].length; tile++) {
          if (this.boardLogic.BoardMatrix[row][tile] !== undefined) {
            drawBorder(context, this.boardLogic.BoardMatrix[row][tile]);
            context.fillStyle = this.boardLogic.BoardMatrix[row][tile].color;
            context.fillRect(this.boardLogic.BoardMatrix[row][tile].xPosition, this.boardLogic.BoardMatrix[row][tile].yPosition, this.boardLogic.BoardMatrix[row][tile].width, this.boardLogic.BoardMatrix[row][tile].height);
          }
        }
      }

      function drawBorderForPreview(context, block) {
        var thickness = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
        context.fillStyle = 'White';
        context.fillRect(block.previewX - thickness, block.previewY - thickness, block.previewWidth + thickness * 2, block.previewHeight + thickness * 2);
      }

      function drawBorder(context, block) {
        var thickness = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
        context.fillStyle = 'White';
        context.fillRect(block.xPosition - thickness, block.yPosition - thickness, block.width + thickness * 2, block.height + thickness * 2);
      }
    }
  }, {
    key: "keyHandle",
    value: function keyHandle(keyPressed, canvas) {
      if (this.livePiece !== undefined && this.VALID_KEYS.indexOf(keyPressed.toLowerCase()) != -1) {
        this.livePiece.movementHandler(keyPressed.toLowerCase(), canvas, this.boardLogic); //although movementHandler is not overriden in TBlock its still a member

        if (this.livePiece.hitBottom) {
          this.boardLogic.addPieceToBoard(this.livePiece);
          this.boardLogic.handleRowCleanup();
          this.livePiece = undefined;
          return;
        }
      }
    }
  }, {
    key: "update",
    value: function update() {
      if (this.livePiece === undefined) {
        var piece;

        if (!this.firstPieceSelected) {
          piece = this.getPiece();
        } else {
          piece = this.nextPiece;
        }

        this.livePiece = piece;
        this.firstPieceSelected = true;

        if (this.firstPieceSelected) {
          this.nextPiece = this.getPiece();
        }
      } else {
        if (!this.livePiece.hitBottom) {
          this.livePiece.updateBoardPosition();
        }

        if (this.livePiece.hitBottom) {
          this.boardLogic.addPieceToBoard(this.livePiece);
          this.boardLogic.handleRowCleanup();
          this.livePiece = undefined;
          return;
        }
      }
    }
  }, {
    key: "getPiece",
    value: function getPiece() {
      var piece = {
        1: _LeftLPiece.LLPiece,
        2: _RightLPiece.RLPiece,
        3: _SquarePiece.SquarePiece,
        4: _RightZPiece.RightZPiece,
        5: _LeftZPiece.LeftZPiece,
        6: _tBlock.TBlock,
        7: _StickPiece.StickPiece
      };

      if (this.lastTwoPieces.length == 0) {
        var _RANDOM_NUM = Math.floor(Math.random() * 7) + 1;

        this.lastTwoPieces.push(_RANDOM_NUM);
        return new piece[_RANDOM_NUM](GameCanvas.TILE_WIDTH, GameCanvas.TILE_HEIGHT, GameCanvas.BOARD_HEIGHT, this);
      }

      var RANDOM_NUM; //Getting unique piece not in the list already, so pieces arent repeat

      do {
        RANDOM_NUM = Math.floor(Math.random() * 7) + 1;
      } while (this.lastTwoPieces[0] == RANDOM_NUM || this.lastTwoPieces[1] == RANDOM_NUM); //if we only have one piece in the list (this occurs after the first piece, then just add another), else remove one of the pieces and append the other


      if (this.lastTwoPieces.length == 1) {
        this.lastTwoPieces.push(RANDOM_NUM);
        return new piece[RANDOM_NUM](GameCanvas.TILE_WIDTH, GameCanvas.TILE_HEIGHT, GameCanvas.BOARD_HEIGHT, this);
      } else {
        this.lastTwoPieces.push(RANDOM_NUM);
        this.lastTwoPieces.shift();
        return new piece[RANDOM_NUM](GameCanvas.TILE_WIDTH, GameCanvas.TILE_HEIGHT, GameCanvas.BOARD_HEIGHT, this);
      }
    }
  }]);

  return GameCanvas;
}();

exports.GameCanvas = GameCanvas;
GameCanvas.BOARD_WIDTH = 250;
GameCanvas.BOARD_HEIGHT = 400;
GameCanvas.TILE_WIDTH = GameCanvas.BOARD_WIDTH / 10;
GameCanvas.TILE_HEIGHT = GameCanvas.BOARD_HEIGHT / 16;
},{"./LeftLPiece":"LeftLPiece.js","./RightLPiece":"RightLPiece.js","./SquarePiece":"SquarePiece.js","./RightZPiece":"RightZPiece.js","./LeftZPiece":"LeftZPiece.js","./StickPiece":"StickPiece.js","./t-block":"t-block.js","./BoardLogicModel":"BoardLogicModel.js"}],"Main.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.board = exports.MainLoop = void 0;

var _GameCanvas = require("./GameCanvas");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var MainLoop = /*#__PURE__*/function () {
  function MainLoop() {
    _classCallCheck(this, MainLoop);
  }

  _createClass(MainLoop, null, [{
    key: "mainLoop",
    value: function mainLoop(timestamp) {
      if (MainLoop.pause) {
        requestAnimationFrame(MainLoop.mainLoop);
        return;
      }

      if (timestamp < MainLoop.lastFrameTimeMs + 1000 / MainLoop.maxFPS) {
        requestAnimationFrame(MainLoop.mainLoop);
        return;
      }

      MainLoop.lastFrameTimeMs = timestamp;
      board.update();
      board.drawBoard();
      requestAnimationFrame(MainLoop.mainLoop);
    }
  }]);

  return MainLoop;
}();

exports.MainLoop = MainLoop;
MainLoop.lastFrameTimeMs = 0;
MainLoop.maxFPS = 3; // The last time the loop was run, max fps

MainLoop.pause = false;
var canvas = document.getElementById('canvas'); //ge the canvas from HTML document

var previewCanvas = document.getElementById('previewCanvas'); //ge the canvas from HTML document

var board = new _GameCanvas.GameCanvas(canvas, previewCanvas);
exports.board = board;
window.addEventListener('keydown', function (e) {
  if (!MainLoop.pause) {
    board.keyHandle(String.fromCharCode(e.which), board);
  }
}); //setup eventlistener

window.addEventListener('keydown', function (e) {
  if (String.fromCharCode(e.which) == "P") {
    if (MainLoop.pause) {
      MainLoop.pause = false;
      return;
    }

    MainLoop.pause = true;
  }
}); //setup eventlistener

requestAnimationFrame(MainLoop.mainLoop); //dont even need async, just add eventlistener and a function thats binded to it
},{"./GameCanvas":"GameCanvas.js"}],"../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "43905" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","Main.js"], null)
//# sourceMappingURL=/Main.edc4ea10.js.map