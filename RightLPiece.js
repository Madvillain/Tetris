import { PieceAbstraction } from "./PieceAbstraction";
export class RLPiece extends PieceAbstraction {
    constructor(TILE_WIDTH, TILE_HEIGHT, BOARD_HEIGHT, canvas) {
        super();
        this.TILE_WIDTH = TILE_WIDTH;
        this.TILE_HEIGHT = TILE_HEIGHT;
        this.BOARD_HEIGHT = BOARD_HEIGHT;
        this.canvas = canvas;
        this.COLOR = "Red";
        //Default block positions on construction
        this.topBlock = {
            previewY: 25,
            previewX: 25,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 5,
            yPosition: 0,
            width: this.TILE_HEIGHT,
            height: this.TILE_WIDTH,
            color: this.COLOR
        };
        this.btmLeftBlock = {
            previewY: 50,
            previewX: 25,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 3,
            yPosition: this.TILE_HEIGHT,
            width: this.TILE_HEIGHT,
            height: this.TILE_WIDTH,
            color: this.COLOR
        };
        this.btmCenterBlock = {
            previewY: 50,
            previewX: 50,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_HEIGHT * 4,
            yPosition: this.TILE_WIDTH,
            width: this.TILE_HEIGHT,
            height: this.TILE_WIDTH,
            color: this.COLOR
        };
        this.btmRightBlock = {
            previewY: 50,
            previewX: 75,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 5,
            yPosition: this.TILE_HEIGHT,
            width: this.TILE_HEIGHT,
            height: this.TILE_WIDTH,
            color: this.COLOR
        };
        this.pieceConstructed = { amountOfBlocks: 4, color: this.COLOR, blocksContainer: [this.topBlock, this.btmLeftBlock, this.btmCenterBlock, this.btmRightBlock] };
        this.pieceInitalized = true;
        this.pieceRotationProperties = { isRotatable: true, axisOfRotation: this.btmCenterBlock };
    }
    updateBoardPosition() {
        if (this.pieceInitalized) {
            super.downMovement(this.canvas); //check if it'll hit bounds
        }
    }
    getPiece() {
        return this.pieceConstructed;
    }
}
