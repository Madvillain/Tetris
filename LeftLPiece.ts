import { PieceAbstraction } from "./PieceAbstraction";
import type {TetrisPiece} from "./IPieceMeta";
import {GameCanvas} from './GameCanvas'
import type {Block} from './IPieceMeta';


export class LLPiece extends PieceAbstraction{


    public pieceInitalized: boolean;
    public hitBottom: boolean;
    public leftEdge: number;
    public rightEdge: number;
    public bottomEdge: number;
    private readonly COLOR : string = "Black";


    public pieceRotationProperties;
    
    //Default block positions on construction
    private topBlock: Block = {
        previewY : 25,
        previewX : 25,
        previewWidth : 25,
        previewHeight : 25,

        xPosition: this.TILE_WIDTH * 3,
        yPosition: 0,
        width: this.TILE_HEIGHT,
        height: this.TILE_WIDTH,
        color : this.COLOR
    }
    private btmLeftBlock: Block = {
        previewY : 50,
        previewX : 25,
        previewWidth : 25,
        previewHeight : 25,


        xPosition: this.TILE_WIDTH * 3,
        yPosition: this.TILE_HEIGHT,
        width: this.TILE_HEIGHT,
        height: this.TILE_WIDTH,
        color : this.COLOR
    }
    private btmCenterBlock: Block = {
        previewY : 50,
        previewX : 50,
        previewWidth : 25,
        previewHeight : 25,

        xPosition: this.TILE_WIDTH * 4,
        yPosition:  this.TILE_WIDTH,
        width: this.TILE_HEIGHT,
        height: this.TILE_WIDTH,
        color : this.COLOR
    }
    private btmRightBlock: Block = {
        previewY : 50,
        previewX : 75,
        previewWidth : 25,
        previewHeight : 25,

        xPosition: this.TILE_WIDTH * 5,
        yPosition: this.TILE_HEIGHT,
        width: this.TILE_HEIGHT,
        height: this.TILE_WIDTH,
        color : this.COLOR
    }


    public pieceConstructed : TetrisPiece = {amountOfBlocks : 4, color : this.COLOR, blocksContainer : [this.topBlock, this.btmLeftBlock, this.btmCenterBlock, this.btmRightBlock]};

    
    constructor(private readonly TILE_WIDTH : number, private readonly TILE_HEIGHT : number, private readonly BOARD_HEIGHT, private readonly canvas : GameCanvas) {
        super();
        this.pieceInitalized = true;
        this.pieceRotationProperties  = {isRotatable : true, axisOfRotation : this.btmCenterBlock};
    }

    updateBoardPosition() : void {
        if (this.pieceInitalized){
            super.downMovement(this.canvas); //check if it'll hit bounds
        }   
    }

    public getPiece() : TetrisPiece {
        return this.pieceConstructed;
    }
 

}
