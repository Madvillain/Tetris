import type { TetrisPiece, Block } from './IPieceMeta';
import type { PieceAbstraction } from './PieceAbstraction';
import { LLPiece } from './LeftLPiece'
import { RLPiece } from './RightLPiece';
import { SquarePiece } from './SquarePiece';
import { RightZPiece } from './RightZPiece';
import { LeftZPiece } from './LeftZPiece';
import { StickPiece } from './StickPiece'
import { TBlock } from './t-block';
import { boardLogicModel } from './BoardLogicModel';
import { CoordinatePair } from './BoardLogicModel';


export class GameCanvas {
  static BOARD_WIDTH: number = 250;
  static BOARD_HEIGHT: number = 400;

  static TILE_WIDTH = GameCanvas.BOARD_WIDTH / 10;
  static TILE_HEIGHT = GameCanvas.BOARD_HEIGHT / 16;

  readonly LEFT_MARGIN: number = 0;
  readonly RIGHT_MARGIN: number = GameCanvas.BOARD_WIDTH - GameCanvas.TILE_WIDTH;
  readonly BOTTOM_MARGIN: number = GameCanvas.BOARD_HEIGHT - GameCanvas.TILE_HEIGHT;

  readonly VALID_KEYS = ['w', 'a', 's', 'd', 'e'];
  private livePiece: PieceAbstraction = undefined;
  private lastTwoPieces: number[] = [];

  private LOCK: boolean = false;
  private lastMoveTaken: boolean = false;

  private firstPieceSelected: boolean = false;
  private nextPiece: PieceAbstraction;

  public boardLogic: boardLogicModel;

  constructor(private canvas: HTMLCanvasElement, private previewCanvas: HTMLCanvasElement) {
    //Main Canvas
    this.canvas.height = GameCanvas.BOARD_HEIGHT;
    this.canvas.width = GameCanvas.BOARD_WIDTH;

    //PreviewCanvas
    this.previewCanvas.height = 125;
    this.previewCanvas.width = 125;

    this.boardLogic = new boardLogicModel(GameCanvas.TILE_HEIGHT, GameCanvas.TILE_WIDTH); //initalizing boardLogicController
    this.drawBoard();
  }

  //Draws board with tiles
  public drawBoard(): void {

    //Setup Preview
    let previewContext: CanvasRenderingContext2D = this.previewCanvas.getContext("2d");
    previewContext.clearRect(0, 0, 125, 125);

    if (this.nextPiece) {
      for (let i = 0; i < this.nextPiece.pieceConstructed.amountOfBlocks; i++) {
        drawBorderForPreview(previewContext, this.nextPiece.pieceConstructed.blocksContainer[i]);
        previewContext.fillStyle = this.nextPiece.pieceConstructed.color ?? "Black";
        previewContext.fillRect(
          this.nextPiece.pieceConstructed.blocksContainer[i].previewX,
          this.nextPiece.pieceConstructed.blocksContainer[i].previewY,
          this.nextPiece.pieceConstructed.blocksContainer[i].previewWidth,
          this.nextPiece.pieceConstructed.blocksContainer[i].previewHeight
        );        
      }
    }
    

    //Draw Main Board on Canvas
    let context: CanvasRenderingContext2D = this.canvas.getContext("2d");
    context.clearRect(0, 0, GameCanvas.BOARD_WIDTH, GameCanvas.BOARD_HEIGHT);

    let squareWidth: number = GameCanvas.BOARD_WIDTH / 10;
    let sqaureHeight: number = GameCanvas.BOARD_HEIGHT / 16;

    let yPosition: number = 0;

    for (let i = 0; i < 16; i++) {
      let xPosition: number = 0;
      for (let j = 0; j < 10; j++) {
        context.strokeStyle = "Black"
        context.strokeRect(xPosition, yPosition, squareWidth, sqaureHeight);
        if (xPosition <= GameCanvas.BOARD_WIDTH - 25) {
          xPosition += squareWidth;
        } else {
          //we've drawn the whole row so now move onto next
          break;
        }
      }
      yPosition += sqaureHeight;
    }


    //Draw Pieces coming down
    if (this.livePiece !== undefined) {
      for (let i = 0; i < this.livePiece.pieceConstructed.amountOfBlocks; i++) {
        drawBorder(context, this.livePiece.pieceConstructed.blocksContainer[i]);

        context.fillStyle = this.livePiece.pieceConstructed.color ?? "Black";
        context.fillRect(
          this.livePiece.pieceConstructed.blocksContainer[i].xPosition,
          this.livePiece.pieceConstructed.blocksContainer[i].yPosition,
          this.livePiece.pieceConstructed.blocksContainer[i].width,
          this.livePiece.pieceConstructed.blocksContainer[i].height
        );
      }

    }

    //Draw blocks already set
    for (let row = 0; row < this.boardLogic.BoardMatrix.length; row++) {
      for (let tile = 0; tile < this.boardLogic.BoardMatrix[row].length; tile++) {
        if (this.boardLogic.BoardMatrix[row][tile] !== undefined) {
          drawBorder(context, this.boardLogic.BoardMatrix[row][tile]);
          context.fillStyle = this.boardLogic.BoardMatrix[row][tile].color;
          context.fillRect(this.boardLogic.BoardMatrix[row][tile].xPosition, this.boardLogic.BoardMatrix[row][tile].yPosition, this.boardLogic.BoardMatrix[row][tile].width, this.boardLogic.BoardMatrix[row][tile].height)
        }
      }
    }


    function drawBorderForPreview(context: CanvasRenderingContext2D, block: Block, thickness = 1): void {//context is reference type{
      context.fillStyle = 'White';
      context.fillRect(
        block.previewX - (thickness),
        block.previewY - (thickness),
        block.previewWidth + (thickness * 2),
        block.previewHeight + (thickness * 2)
      );
    }

    function drawBorder(context: CanvasRenderingContext2D, block: Block, thickness = 1): void //context is reference type
    {
      context.fillStyle = 'White';
      context.fillRect(
        block.xPosition - (thickness),
        block.yPosition - (thickness),
        block.width + (thickness * 2),
        block.height + (thickness * 2)
      );
    }
  }


  public keyHandle(keyPressed: string, canvas: GameCanvas): void {
    if (this.livePiece !== undefined && this.VALID_KEYS.indexOf(keyPressed.toLowerCase()) != -1) {
      this.livePiece.movementHandler(keyPressed.toLowerCase(), canvas, this.boardLogic); //although movementHandler is not overriden in TBlock its still a member
      if (this.livePiece.hitBottom) {
        this.boardLogic.addPieceToBoard(this.livePiece);
        this.boardLogic.handleRowCleanup();
        this.livePiece = undefined;
        return;
      }
    }
  }


  public update(): void {
    if (this.livePiece === undefined) {
      let piece: PieceAbstraction;
      
      if (!this.firstPieceSelected) {
        piece = this.getPiece();  
      } else {
        piece = this.nextPiece;
      }
      this.livePiece = piece;
      this.firstPieceSelected = true;
      if (this.firstPieceSelected) {
        this.nextPiece = this.getPiece();   
      }

    } else {
      if (!this.livePiece.hitBottom) {
        this.livePiece.updateBoardPosition();
      }

      if (this.livePiece.hitBottom) {
        this.boardLogic.addPieceToBoard(this.livePiece);
        this.boardLogic.handleRowCleanup();
        this.livePiece = undefined;
        return;
      }
    }
  }





  public getPiece(): PieceAbstraction {
    let piece = { 1: LLPiece, 2: RLPiece, 3: SquarePiece, 4: RightZPiece, 5: LeftZPiece, 6: TBlock, 7: StickPiece };

    if (this.lastTwoPieces.length == 0) {
      const RANDOM_NUM = Math.floor(Math.random() * 7) + 1
      this.lastTwoPieces.push(RANDOM_NUM);
      return new piece[RANDOM_NUM](GameCanvas.TILE_WIDTH, GameCanvas.TILE_HEIGHT, GameCanvas.BOARD_HEIGHT, this);
    }

    let RANDOM_NUM;

    //Getting unique piece not in the list already, so pieces arent repeat
    do {
      RANDOM_NUM = Math.floor(Math.random() * 7) + 1
    } while (this.lastTwoPieces[0] == RANDOM_NUM || this.lastTwoPieces[1] == RANDOM_NUM);

    //if we only have one piece in the list (this occurs after the first piece, then just add another), else remove one of the pieces and append the other
    if (this.lastTwoPieces.length == 1) {
      this.lastTwoPieces.push(RANDOM_NUM);
      return new piece[RANDOM_NUM](GameCanvas.TILE_WIDTH, GameCanvas.TILE_HEIGHT, GameCanvas.BOARD_HEIGHT, this);
    } else {
      this.lastTwoPieces.push(RANDOM_NUM);
      this.lastTwoPieces.shift();
      return new piece[RANDOM_NUM](GameCanvas.TILE_WIDTH, GameCanvas.TILE_HEIGHT, GameCanvas.BOARD_HEIGHT, this);
    }
  }
}