import { PieceAbstraction } from "./PieceAbstraction";
import type {TetrisPiece} from "./IPieceMeta";
import {GameCanvas} from './GameCanvas'
import type {Block} from './IPieceMeta';


export class RightZPiece extends PieceAbstraction {    
    public pieceInitalized: boolean;
    public hitBottom: boolean;
    public leftEdge: number;
    public rightEdge: number;
    public bottomEdge: number;

    private readonly COLOR : string = "Blue";
    
    public pieceRotationProperties;
    

    public pieceConstructed : TetrisPiece;

    private topRightBlock: Block = {
        previewY : 25,
        previewX : 75,
        previewWidth : 25,
        previewHeight : 25,


        xPosition : this.TILE_WIDTH * 6,
        yPosition : 0,
        width : this.TILE_WIDTH,
        height : this.TILE_HEIGHT,
        color : this.COLOR
    }   

    private topMiddleBlock: Block = {
        previewY : 25,
        previewX : 50,
        previewWidth : 25,
        previewHeight : 25,

        xPosition : this.TILE_WIDTH * 5,
        yPosition : 0,
        width : this.TILE_WIDTH,
        height : this.TILE_HEIGHT,
        color : this.COLOR
    }

    private bottomMiddleBlock: Block = {
        previewY : 50,
        previewX : 50,
        previewWidth : 25,
        previewHeight : 25,

        xPosition : this.TILE_WIDTH * 5,
        yPosition : this.TILE_HEIGHT,
        width : this.TILE_WIDTH,
        height : this.TILE_HEIGHT,
        color : this.COLOR
    }

    private bottomLeftBlock: Block = {
        previewY : 50,
        previewX : 25,
        previewWidth : 25,
        previewHeight : 25,

        xPosition : this.TILE_WIDTH * 4,
        yPosition : this.TILE_HEIGHT,
        width : this.TILE_WIDTH,
        height : this.TILE_HEIGHT,
        color : this.COLOR
    }

    constructor(private readonly TILE_WIDTH : number, private readonly TILE_HEIGHT : number, private readonly BOARD_HEIGHT, private readonly canvas : GameCanvas) {
        super();
        this.pieceConstructed = {amountOfBlocks : 4, color : this.COLOR, blocksContainer : [this.topRightBlock, this.topMiddleBlock, this.bottomMiddleBlock, this.bottomLeftBlock]};
        this.pieceRotationProperties  = {isRotatable : true, axisOfRotation : this.bottomMiddleBlock};
        this.pieceInitalized = true;
    }

    updateBoardPosition() : void {
        if (this.pieceInitalized){
            super.downMovement(this.canvas);
        }   
    }

    getPiece(): TetrisPiece {
        return this.pieceConstructed;
    }
    

}
