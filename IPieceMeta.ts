/**All the required information for any given tetris piece */
export type Block = {
  previewY ?: number,
  previewX?: number,
  previewWidth?: number,
  previewHeight?: number,

  yPosition: number,
  xPosition: number,
  width: number,
  height: number,
  color: string;
}

export type TetrisPiece = {
  amountOfBlocks : number,
  blocksContainer : Block[]
  speed? : number;
  color? : string;
}


                                  
