import { PieceAbstraction } from "./PieceAbstraction";
export class StickPiece extends PieceAbstraction {
    constructor(TILE_WIDTH, TILE_HEIGHT, BOARD_HEIGHT, canvas) {
        super();
        this.TILE_WIDTH = TILE_WIDTH;
        this.TILE_HEIGHT = TILE_HEIGHT;
        this.BOARD_HEIGHT = BOARD_HEIGHT;
        this.canvas = canvas;
        this.COLOR = "Teal";
        this.topBlock = {
            previewY: 25,
            previewX: 50,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 4,
            yPosition: 0,
            width: this.TILE_WIDTH,
            height: this.TILE_HEIGHT,
            color: this.COLOR
        };
        this.secondBlock = {
            previewY: 50,
            previewX: 50,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 4,
            yPosition: this.TILE_HEIGHT,
            width: this.TILE_WIDTH,
            height: this.TILE_HEIGHT,
            color: this.COLOR
        };
        this.middleBlock = {
            previewY: 75,
            previewX: 50,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 4,
            yPosition: this.TILE_HEIGHT * 2,
            width: this.TILE_WIDTH,
            height: this.TILE_HEIGHT,
            color: this.COLOR
        };
        this.bottomBlock = {
            previewY: 100,
            previewX: 50,
            previewWidth: 25,
            previewHeight: 25,
            xPosition: this.TILE_WIDTH * 4,
            yPosition: this.TILE_HEIGHT * 3,
            width: this.TILE_WIDTH,
            height: this.TILE_HEIGHT,
            color: this.COLOR
        };
        this.pieceConstructed = { amountOfBlocks: 4, color: this.COLOR, blocksContainer: [this.topBlock, this.secondBlock, this.middleBlock, this.bottomBlock] };
        this.pieceRotationProperties = { isRotatable: true, axisOfRotation: this.middleBlock };
        this.pieceInitalized = true;
    }
    updateBoardPosition() {
        if (this.pieceInitalized) {
            super.downMovement(this.canvas);
        }
    }
    getPiece() {
        return this.pieceConstructed;
    }
}
