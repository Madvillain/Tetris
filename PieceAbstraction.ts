import type { TetrisPiece, Block } from './IPieceMeta';
import { GameCanvas } from './GameCanvas';
import {boardLogicModel} from './BoardLogicModel';
import {MatrixEntry} from './BoardLogicModel';
import { SquarePiece } from './SquarePiece';
/**
 * If a new tetris piece class is created we need to define the outer most blocks for bounds checking. They shoudl assigned in the constrctor method of the class, and their values correspond to which blocks in the 
 * pieceConstructed.blocksContainer array represent the OUTER_MOST_RIGHT_BLOCK, OUTER_MOST_LEFT_BLOCK, OUTER_MOST_BOTTOM_BLOCK
 * 
 * In other words, look at your TetrisPiece you are constructing and define the OUTER_MOST_<T>_BLOCK by the index of which it is in the this.pieceConstructed.blocksContainer
 */



export type rotationProprties = {
    isRotatable : boolean;
    axisOfRotation : Block;
}


//Why abstract: because i want my pieces to inherit but i dont want this class itself to have an instance
export abstract class PieceAbstraction {
    //In TS these properties are public by default but its nice to keep it explicit for the reader
    public abstract pieceConstructed: TetrisPiece;
    public abstract pieceInitalized: boolean = false;
    public abstract hitBottom: boolean = false;
    public abstract leftEdge: number;
    public abstract rightEdge: number;
    public abstract bottomEdge: number;

    public abstract pieceRotationProperties : rotationProprties; 

    abstract getPiece(): TetrisPiece;
    abstract updateBoardPosition(): void;


    /**Movement will be written here just pass blocks container and call super.leftMovement */
    public movementHandler(keyPress: string, canvas: GameCanvas, boardLogic : boardLogicModel) {
        switch (keyPress) {
            case "a": {
                this.leftMovement(canvas);
                break;
            }
            case "s": {
                this.downMovement(canvas);
                break;
            }
            case "d": {
                this.rightMovement(canvas);
                break;
            }
            case "w": {
                this.rotatePiece(canvas, boardLogic);
                break;
            }
            default:
                break;
        }
    }
    
    
    //These non-abstract methods are members of the implementer since they are inherited. So when we call `this` we are referencing the caller class. So we can access abstract properties from the caller
    
    private leftMovement(canvas: GameCanvas) {
        //Canvas Bounds Check
        for (let block of this.pieceConstructed.blocksContainer){
            if (block.xPosition - GameCanvas.TILE_WIDTH < canvas.LEFT_MARGIN) {
                return;
            }
        }

        //Checking collision with another piece on the board to the left
        for (let block of this.pieceConstructed.blocksContainer){
            let blockRowIndex = canvas.boardLogic.getMatrixY(block.yPosition);
            let blockTileIndex = canvas.boardLogic.getMatrixY(block.xPosition);

            if (canvas.boardLogic.BoardMatrix[blockRowIndex][blockTileIndex - 1] !== undefined){
                return;
            }
        }

    
        for (let i = 0; i < this.pieceConstructed.blocksContainer.length; i++) {
            this.pieceConstructed.blocksContainer[i].xPosition -= GameCanvas.TILE_HEIGHT;
            canvas.drawBoard();
        }
    }

    
    private rightMovement(canvas: GameCanvas) {
        //Canvas Bounds Check
        for (let block of this.pieceConstructed.blocksContainer){
            if (block.xPosition + GameCanvas.TILE_WIDTH > canvas.RIGHT_MARGIN) {
                return;
            }
        }
        //Checking collision with another piece on the board to the left
        for (let block of this.pieceConstructed.blocksContainer){
            let blockRowIndex = canvas.boardLogic.getMatrixY(block.yPosition);
            let blockTileIndex = canvas.boardLogic.getMatrixY(block.xPosition);

            //This works because the piece isnt on the board yet
            if (canvas.boardLogic.BoardMatrix[blockRowIndex][blockTileIndex + 1] !== undefined){
                return;
            }
        }

        for (let i = 0; i < this.pieceConstructed.blocksContainer.length; i++) {
            this.pieceConstructed.blocksContainer[i].xPosition += GameCanvas.TILE_HEIGHT;
            canvas.drawBoard();
        }
    }



    public downMovement(canvas: GameCanvas) {    
        //Canvas Bounds Check
        for (let block of this.pieceConstructed.blocksContainer){
            if (block.yPosition + GameCanvas.TILE_HEIGHT > canvas.BOTTOM_MARGIN){
                this.hitBottom = true;
                return;
            }
        }

        for (let block of this.pieceConstructed.blocksContainer){
            let blockRowIndex = canvas.boardLogic.getMatrixY(block.yPosition);
            let blockTileIndex = canvas.boardLogic.getMatrixY(block.xPosition);

            if (canvas.boardLogic.BoardMatrix[blockRowIndex + 1][blockTileIndex] !== undefined){
                this.hitBottom = true;
                return;
            }
        }
            
        for (let block of this.pieceConstructed.blocksContainer){
            block.yPosition += GameCanvas.TILE_HEIGHT;
            canvas.drawBoard();
        }
    }

  


            
    private rotatePiece(canvas: GameCanvas, boardLogic : boardLogicModel) {
        //Square is -1 because no point in rotating it
        if (this.hitBottom){
            return;
        }

        if (!this.pieceRotationProperties.isRotatable){
            return;
        }

        //Making a copy just in case not all pieces are valid rotations
        const originalPiece = {amountOfBlocks : this.pieceConstructed.amountOfBlocks, blocksContainer : this.pieceConstructed.blocksContainer, speed : this.pieceConstructed.speed, color : this.pieceConstructed.color}
      
        for (let block of this.pieceConstructed.blocksContainer){
            const blockVector = boardLogic.vectorizePoint(block);
            const axisOfRotationBlockVector = boardLogic.vectorizePoint(this.pieceRotationProperties.axisOfRotation);

            const newVectorY = blockVector[0] - axisOfRotationBlockVector[0];
            const newVectorX = blockVector[1] - axisOfRotationBlockVector[1];
            const newVector = [newVectorY, newVectorX];

            const dotProduct = this.rotateBy90(newVector);
            const finalVector = [axisOfRotationBlockVector[0] + dotProduct[0], axisOfRotationBlockVector[1] + dotProduct[1]];

            //Have to check if new piece in bounds and doesnt collide
            if (!this.matrixSpaceAvailable(finalVector, boardLogic)){
                this.pieceConstructed = originalPiece;
                return;
            }
            
            //Set new Coordinates from Vector
            const canvasY = finalVector[0] * GameCanvas.TILE_HEIGHT;
            const canvasX = finalVector[1] * GameCanvas.TILE_WIDTH;

            if (canvasY > canvas.BOTTOM_MARGIN || canvasX < 0 || canvasX > canvas.RIGHT_MARGIN){
                this.pieceConstructed = originalPiece;
                return;
            }

            block.xPosition = canvasX;
            block.yPosition = canvasY;
            
        }           
        canvas.drawBoard();

    }




    private rotateBy90(vector : number[]){
        return [vector[0] * 0 + vector[1] *-1, vector[0] * 1 + vector[1] * 0];
    }


    private matrixSpaceAvailable(matrixSpace : number[],  boardLogic : boardLogicModel){
        const y = matrixSpace[0];
        const x = matrixSpace[1];

        if (boardLogic.getMatrix()[y][x] !== undefined){
            return false;
        }
        return true;

    }


    constructor() { }
}
