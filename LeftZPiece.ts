import { PieceAbstraction } from "./PieceAbstraction";
import type {TetrisPiece} from "./IPieceMeta";
import {GameCanvas} from './GameCanvas'
import type {Block} from './IPieceMeta';
import { boardLogicModel, MatrixEntry } from "./BoardLogicModel";



export class LeftZPiece extends PieceAbstraction {    
    public pieceInitalized: boolean;
    public hitBottom: boolean;
    public leftEdge: number;
    public rightEdge: number;
    public bottomEdge: number;
    private readonly COLOR : string = "GREEN";

    public pieceRotationProperties;

    public pieceConstructed : TetrisPiece;

    private topLeftBlock: Block = {
        previewY : 25,
        previewX : 25,
        previewWidth : 25,
        previewHeight : 25,
        
        xPosition : this.TILE_WIDTH * 3,
        yPosition : 0,
        width : this.TILE_WIDTH,
        height : this.TILE_HEIGHT,
        color : this.COLOR
    }   

    private topMiddleBlock: Block = {
        previewY : 25,
        previewX : 50,
        previewWidth : 25,
        previewHeight : 25,
        
        xPosition : this.TILE_WIDTH * 4,
        yPosition : 0,
        width : this.TILE_WIDTH,
        height : this.TILE_HEIGHT,
        color : this.COLOR
    }

    private bottomMiddleBlock: Block = {
        previewY : 50,
        previewX : 50,
        previewWidth : 25,
        previewHeight : 25,
        
        xPosition : this.TILE_WIDTH * 4,
        yPosition : this.TILE_HEIGHT,
        width : this.TILE_WIDTH,
        height : this.TILE_HEIGHT,
        color : this.COLOR
    }

    private bottomRightBlock: Block = {
        previewY : 50,
        previewX : 75,
        previewWidth : 25,
        previewHeight: 25,
        
        xPosition : this.TILE_WIDTH * 5,
        yPosition : this.TILE_HEIGHT,
        width : this.TILE_WIDTH,
        height : this.TILE_HEIGHT,
        color : this.COLOR
    }

    constructor(private readonly TILE_WIDTH : number, private readonly TILE_HEIGHT : number, private readonly BOARD_HEIGHT, private readonly canvas : GameCanvas) {
        super();
        this.pieceConstructed = {amountOfBlocks : 4, color : this.COLOR, blocksContainer : [this.topLeftBlock, this.topMiddleBlock, this.bottomMiddleBlock, this.bottomRightBlock]};
        this.pieceRotationProperties  = {isRotatable : true, axisOfRotation : this.bottomMiddleBlock};
        this.pieceInitalized = true;
    }

    updateBoardPosition() : void {
        if (this.pieceInitalized){
            super.downMovement(this.canvas); //check if it'll hit bounds
        }   
    }

    getPiece(): TetrisPiece {
        return this.pieceConstructed;
    }
    

}
