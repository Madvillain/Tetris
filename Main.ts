import { GameCanvas } from './GameCanvas';

export class MainLoop {
  static lastFrameTimeMs: number = 0;
  static maxFPS: number = 3; // The last time the loop was run, max fps
  static pause: boolean = false;

  static mainLoop(timestamp): void {
    if (MainLoop.pause) {
      requestAnimationFrame(MainLoop.mainLoop);
      return;
    }


    if (timestamp < MainLoop.lastFrameTimeMs + (1000 / MainLoop.maxFPS)) {
      requestAnimationFrame(MainLoop.mainLoop);
      return;
    }
    MainLoop.lastFrameTimeMs = timestamp;

    board.update();
    board.drawBoard();
    requestAnimationFrame(MainLoop.mainLoop);
  }
}


let canvas = document.getElementById('canvas') as HTMLCanvasElement; //ge the canvas from HTML document
let previewCanvas = document.getElementById('previewCanvas') as HTMLCanvasElement; //ge the canvas from HTML document

export let board: GameCanvas = new GameCanvas(canvas, previewCanvas);

window.addEventListener('keydown', function (e) {
  if (!MainLoop.pause) {
    board.keyHandle(String.fromCharCode(e.which), board)
  }
}); //setup eventlistener

window.addEventListener('keydown', function (e) {
  if (String.fromCharCode(e.which) == "P") {
    if (MainLoop.pause) {
      MainLoop.pause = false;
      return;
    }
    MainLoop.pause = true;

  }


}); //setup eventlistener




requestAnimationFrame(MainLoop.mainLoop);


//dont even need async, just add eventlistener and a function thats binded to it