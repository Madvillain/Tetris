import type {Block} from './IPieceMeta';
import type {TetrisPiece} from './IPieceMeta';
import {PieceAbstraction} from './PieceAbstraction'
import { GameCanvas } from './GameCanvas';

//this isnt really neccessary but its a nicer way to call pieceConstructed.blocksContainer array then by indicies of 0,1,2,3 especially. So make sure it's implemented in right order

export class TBlock extends PieceAbstraction {
    public pieceInitalized : boolean = false;
    public hitBottom : boolean = false;
    public leftEdge : number;
    public rightEdge : number;
    public bottomEdge : number;

    private readonly COLOR : string = "Purple";
    
    //Default block positions on construction
    private topBlock: Block = {
        previewY : 25,
        previewX : 50,
        previewWidth : 25,
        previewHeight : 25,

        xPosition: this.TILE_WIDTH * 4,
        yPosition: 0,
        width: this.TILE_HEIGHT,
        height: this.TILE_WIDTH,
        color : this.COLOR
    }
    private btmLeftBlock: Block = {
        previewY : 50,
        previewX : 25,
        previewWidth : 25,
        previewHeight: 25,
        
        xPosition: this.TILE_WIDTH * 3,
        yPosition: this.TILE_HEIGHT,
        width: this.TILE_HEIGHT,
        height: this.TILE_WIDTH,
        color : this.COLOR
    }
    private btmCenterBlock: Block = {
        previewY : 50,
        previewX : 50,
        previewWidth : 25,
        previewHeight : 25,
        

        xPosition: this.TILE_WIDTH * 4,
        yPosition:  this.TILE_WIDTH,
        width: this.TILE_HEIGHT,
        height: this.TILE_WIDTH,
        color : this.COLOR
    }
    private btmRightBlock: Block = {
        previewY: 50,
        previewX  : 75,
        previewWidth : 25,
        previewHeight: 25,
        
        xPosition: this.TILE_WIDTH * 5,
        yPosition: this.TILE_HEIGHT,
        width: this.TILE_HEIGHT,
        height: this.TILE_WIDTH,
        color : this.COLOR
    }

    
    public pieceConstructed : TetrisPiece = {amountOfBlocks : 4, color : this.COLOR, blocksContainer : [this.topBlock, this.btmLeftBlock, this.btmCenterBlock, this.btmRightBlock]};

    public pieceRotationProperties;


    constructor(private readonly TILE_WIDTH : number, private readonly TILE_HEIGHT : number, private readonly BOARD_HEIGHT, private readonly canvas : GameCanvas){
        super();
        this.pieceInitalized = true;

        this.pieceRotationProperties  = {isRotatable : true, axisOfRotation : this.btmCenterBlock};
        
    }
    

    updateBoardPosition() : void {
        if (this.pieceInitalized){
            super.downMovement(this.canvas);
        }   
    }

    public getPiece() : TetrisPiece {
        return this.pieceConstructed;
    }



}

