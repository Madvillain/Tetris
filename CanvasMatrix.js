var CanvasCoordinateDictionary = /** @class */ (function () {
    function CanvasCoordinateDictionary() {
        this.items = { size: 0 };
        this.items["5"] = 5;
        this.items.size++;
    }
    CanvasCoordinateDictionary.prototype.getSize = function () {
        console.log(this.items.size);
    };
    CanvasCoordinateDictionary.prototype.addEntry = function (key, value) {
        var settableKey = this.convertCoordinateToString(key);
        //this.items[settableKey] = value;
    };
    CanvasCoordinateDictionary.prototype.keyExists = function (key) {
        var settableKey = this.convertCoordinateToString(key);
        if (this.items[settableKey] !== undefined) {
            return true;
        }
        return false;
    };
    /**
     *
     * @param key takes Coordinate
     * from [2,1] to "2,1"
     */
    CanvasCoordinateDictionary.prototype.convertCoordinateToString = function (key) {
        return key.x.toString() + "," + key.y.toString();
    };
    return CanvasCoordinateDictionary;
}());
export { CanvasCoordinateDictionary };
