import { PieceAbstraction } from "./PieceAbstraction";
import type {TetrisPiece} from "./IPieceMeta";
import {GameCanvas} from './GameCanvas'
import type {Block} from './IPieceMeta';
import { boardLogicModel } from "./BoardLogicModel";


export class StickPiece extends PieceAbstraction {    
    public pieceInitalized: boolean;
    public hitBottom: boolean;
    public leftEdge: number;
    public rightEdge: number;
    public bottomEdge: number;

    private readonly COLOR : string = "Teal";

    public pieceRotationProperties;


    public pieceConstructed : TetrisPiece;

    private topBlock: Block = {
        previewY : 25,
        previewX : 50,
        previewWidth : 25,
        previewHeight : 25,


        xPosition : this.TILE_WIDTH * 4,
        yPosition : 0,
        width : this.TILE_WIDTH,
        height : this.TILE_HEIGHT,
        color : this.COLOR
    }   

    private secondBlock: Block = {
        previewY : 50,
        previewX : 50,
        previewWidth : 25,
        previewHeight : 25,

        xPosition : this.TILE_WIDTH * 4,
        yPosition : this.TILE_HEIGHT,
        width : this.TILE_WIDTH,
        height : this.TILE_HEIGHT,
        color : this.COLOR
    }

    private middleBlock: Block = {
        previewY : 75,
        previewX : 50,
        previewWidth : 25,
        previewHeight: 25,
        
        xPosition : this.TILE_WIDTH * 4,
        yPosition : this.TILE_HEIGHT * 2,
        width : this.TILE_WIDTH,
        height : this.TILE_HEIGHT,
        color : this.COLOR
    }

    private bottomBlock: Block = {
        previewY : 100,
        previewX : 50,
        previewWidth : 25,
        previewHeight: 25,
        
        xPosition : this.TILE_WIDTH * 4,
        yPosition : this.TILE_HEIGHT * 3,
        width : this.TILE_WIDTH,
        height : this.TILE_HEIGHT,
        color : this.COLOR
    }

    constructor(private readonly TILE_WIDTH : number, private readonly TILE_HEIGHT : number, private readonly BOARD_HEIGHT, private readonly canvas : GameCanvas) {
        super();
        this.pieceConstructed = {amountOfBlocks : 4, color : this.COLOR, blocksContainer : [this.topBlock, this.secondBlock, this.middleBlock, this.bottomBlock]};
        this.pieceRotationProperties  = {isRotatable : true, axisOfRotation : this.middleBlock};
        this.pieceInitalized = true;
    }

    updateBoardPosition() : void {
        if (this.pieceInitalized){
            super.downMovement(this.canvas);
        }   
    }

    getPiece(): TetrisPiece {
        return this.pieceConstructed;
    }
    

}
